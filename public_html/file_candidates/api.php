<?php

ini_set('memory_limit','1500M');

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); #|E_ALL
ini_set('display_errors', 'On');

require_once ( '/data/project/fist/FileCandidates.php') ;

$fc = new FileCandidates ;

function micDrop () {
	global $out ;
	header('Content-type: application/json; charset=utf-8');
	if ( isset($_REQUEST['callback'])) print $_REQUEST['callback'].'(' ;
	print json_encode($out) ;
	if ( isset($_REQUEST['callback'])) print ')' ;
	ob_end_flush() ;
}

function dieWithError ( $msg ) {
	global $out ;
	$out['status'] = $msg ;
	micDrop() ;
	exit(0) ;
}

function addRequestedMetadataToOutput () {
	global $out , $fc ;
	$out['meta'] = (object) [] ;
	$meta = get_request ( 'meta' , '' ) ;
	if ( $meta=='all' or preg_match ( '/\bfield_values\b/' , $meta ) ) {
		$out['meta']->field_values = [] ;
		$sql = "SHOW COLUMNS FROM `file_candidates` WHERE Field IN ('status','group','source','file_type')" ;
		$result = getSQL ( $fc->dbt , $sql ) ;
		while($o = $result->fetch_object()){
			if ( !preg_match ( '/^enum\((.+)\)$/' , $o->Type , $m ) ) continue ;
			$field_list = explode ( ',' , str_replace ( "'" , '' , $m[1] ) ) ;
			asort ( $field_list ) ; // Huh, doesn't work?
			$out['meta']->field_values[$o->Field] = [
				'values' => $field_list ,
				'default' => $o->Default
			] ;
		}
	}

}

$out = [ 'data'=>[] , 'status'=>'OK' ] ;
addRequestedMetadataToOutput() ;
$action = get_request ( 'action' , '' ) ;
if ( $action == 'get_candidates' ) {
	$batch_size = get_request ( 'size' , '50' ) * 1 ;
	$params = ['status'=>'WAIT'] ;
	foreach ( ['source','group','file_type'] AS $key ) {
		$params[$key] = explode ( ',' , strtoupper ( get_request ( $key , '' ) ) ) ;
	}
	foreach ( ['sparql','commonscat'] AS $key ) {
		$params[$key] = get_request ( $key , '' ) ;
	}
//	$out['params'] = $params ; // TESTING

	$items = get_request ( 'items' , '' ) ;
	if ( $items == '' ) {
		$items = $fc->getRandomItems($batch_size,$params) ;
		if ( count($items) == 0 ) $items = $fc->getRandomItems($batch_size,$params,true) ;
		if ( count($items) == 0 and !isset($out['warning']) ) $out['warning'] = 'There appear to be no image candidates for this parameter combination' ;
		$out['num_results'] = count($items) ;
	} else {
		$items = explode ( ',' , strtoupper ( $items ) ) ;
		$batch_size = count ( $items ) ;
	}
	
	$item2files = $fc->getFileCandidatesForItems($items,$params) ;
	$out['data'] = $item2files ;

} else if ( $action == 'stats' ) {

	$data = [] ;
	$sql = "SELECT `group`,`status`,`source`,count(*) AS cnt FROM file_candidates GROUP BY `group`,`status`,`source`" ;
	$result = $fc->tfc->getSQL ( $fc->dbt , $sql ) ;
	while ( $o = $result->fetch_object() ) {
		if ( $o->group == '' ) continue ;
		$data[$o->group][$o->status][$o->source] = $o->cnt * 1 ;
	}
	$out['data'] = $data ;


} else if ( $action == 'get_proposed_items' ) {

	$batch_size = get_request ( 'size' , '50' ) * 1 ;
	$params = ['status'=>'WAIT'] ;
	foreach ( ['source','group','file_type'] AS $key ) {
		$params[$key] = explode ( ',' , strtoupper ( get_request ( $key , '' ) ) ) ;
	}
	$params['q_zero'] = 1 ;

	$out['data'] = $fc->getRandomItemCandidates($batch_size,$params) ;

} else if ( $action == 'get_flickr_key' ) {

	$out['data'] = trim ( file_get_contents ( '/data/project/fist/flickr_key.txt' ) ) ;

} else if ( $action == 'set_candidate_status' ) {

	$id = get_request ( 'id' , 0 ) * 1 ;
	if ( $id == 0 ) dieWithError ( 'No candidate ID given' ) ;
	$user = trim ( str_replace ( '_' , ' ' , get_request ( 'user' , '' ) ) ) ;
	if ( $user == '' ) dieWithError ( 'No user name given' ) ;
	$status = trim ( strtoupper ( get_request ( 'status' , '' ) ) ) ;
	if ( $status == '' ) dieWithError ( 'No status given' ) ;
	micDrop() ;

	$prop = preg_replace ( '/\D/' , '' , get_request ( 'prop' , '0' ) ) * 1 ;
	$note = trim ( get_request ( 'note' , '' ) ) ;
	$timestamp = $fc->getTimestamp() ;

	$sql = "UPDATE file_candidates SET `used_for_property`=$prop" ;
	$sql .= ",`status`='" . $fc->escape($status) . "'" ;
	$sql .= ",`user`='" . $fc->escape($user) . "'" ;
	$sql .= ",`timestamp`='" . $fc->escape($timestamp) . "'" ;
	if ( $note != '' ) $sql .= ",`note`='" . $fc->escape($note) . "'" ;
	$sql .= " WHERE id=$id" ;
	getSQL ( $fc->dbt , $sql ) ;
	$fc->logFileEvent ( $id , ($status=='DONE')?'MATCHED':'OTHER_FILE_USED' , 'USER' ) ;
	exit(0) ;

} else {

	require_once '/data/project/magnustools/public_html/php/Widar.php' ;
	$widar = new \Widar ( 'fist' ) ;
	$widar->attempt_verification_auto_forward ( 'https://fist.toolforge.org/file_candidates' ) ;
	$widar->authorization_callback = 'https://fist.toolforge.org/file_candidates/api.php' ;
	if ( $widar->render_reponse ( true ) ) exit ( 0 ) ;
	$out['status'] = "Unknown action '$action'" ;
}


micDrop() ;

?>