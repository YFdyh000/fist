<?php

ini_set('memory_limit','1500M');

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); #|E_ALL
ini_set('display_errors', 'On');

require_once ( '/data/project/fist/FileCandidates.php') ;

$tfc = new ToolforgeCommon ( 'wd4wp' ) ;
$dbt = $tfc->openDBtool ( 'wd4wp_p' ) ;

function micDrop () {
	global $out ;
	header('Content-type: application/json; charset=utf-8');
	#if ( isset($_REQUEST['callback'])) print $_REQUEST['callback'].'(' ;
	print json_encode($out) ;
	#if ( isset($_REQUEST['callback'])) print ')' ;
	ob_end_flush() ;
	exit(0) ;
}

function dieWithError ( $msg ) {
	global $out ;
	$out['status'] = $msg ;
	micDrop() ;
}

$out = [ 'status'=>'OK' , 'data' => [] ] ;
$action = get_request ( 'action' , '' ) ;
if ( $action == 'get_wikis' ) {

	$sql = "SELECT name,count(*) AS cnt FROM wiki,page where page.wiki=wiki.id AND page.status='TODO' GROUP BY wiki ORDER BY name" ;
	try {
		$result = $tfc->getSQL ( $dbt , $sql ) ;
	} catch (Exception $e) {
		dieWithError ( $e->getMessage() ) ;
	}
	while ( $o = $result->fetch_object() ) {
		$o->server = $tfc->getWebserverForWiki ( $o->name ) ;
		$out['data'][$o->name] = $o ;
	}

} else if ( $action == 'get_p31s' ) {

	$wiki = trim ( strtolower( $tfc->getRequest ( 'wiki' , '' ) ) ) ;
	if ( $wiki == '' ) dieWithError ( 'No wiki given' ) ;
	$wiki = $dbt->real_escape_string ( $wiki ) ;
	$min = $tfc->getRequest ( 'min' , 20 ) * 1 ;

	$sql = "SELECT p31,count(*) AS cnt FROM page_instance WHERE page_id IN (SELECT id FROM page WHERE `status`='TODO' AND wiki IN (SELECT id FROM wiki WHERE name='{$wiki}')) GROUP BY p31 HAVING cnt>={$min} ORDER BY cnt DESC" ;
	try {
		$result = $tfc->getSQL ( $dbt , $sql ) ;
	} catch (Exception $e) {
		dieWithError ( $e->getMessage() ) ;
	}
	while ( $o = $result->fetch_object() ) $out['data'][] = $o ;

} else if ( $action == 'get_candidates' ) {

	$wiki = trim ( strtolower( $tfc->getRequest ( 'wiki' , '' ) ) ) ;
	if ( $wiki == '' ) dieWithError ( 'No wiki given' ) ;
	$wiki = $dbt->real_escape_string ( $wiki ) ;
	$limit = $tfc->getRequest ( 'limit' , 20 ) * 1 ;
	if ( $limit <= 0 or $limit > 100 ) dieWithError ( 'Bad limit' ) ;
	$offset = $tfc->getRequest ( 'offset' , 0 ) * 1 ;
	if ( $offset < 0 ) dieWithError ( 'Bad offset' ) ;
	$p31_filter = $tfc->getRequest ( 'p31_filter' , '0' ) * 1 ;

	$sql = "SELECT * FROM `page` WHERE `wiki` IN (SELECT `id` FROM `wiki` WHERE name='{$wiki}')" ;
	if ( $p31_filter > 0 ) $sql .= " AND EXISTS (SELECT * FROM `page_instance` WHERE `page_id`=`page`.`id` AND `p31`={$p31_filter})" ;
	$sql .= " AND `status`='TODO' ORDER BY `id` LIMIT {$limit} OFFSET {$offset}" ;
	try {
		$result = $tfc->getSQL ( $dbt , $sql ) ;
	} catch (Exception $e) {
		dieWithError ( $e->getMessage() ) ;
	}
	while ( $o = $result->fetch_object() ) $out['data'][] = $o ;

} else if ( $action == 'change_status' ) {

	$candidate_id = $tfc->getRequest ( 'candidate_id' , '0' ) * 1 ;
	if ( $candidate_id <= 0 ) dieWithError ( 'Bad candidate_id' ) ;
	$new_status = trim ( strtoupper ( $dbt->real_escape_string ( $tfc->getRequest ( 'new_status' , '' ) ) ) );
	if ( $new_status == '' ) dieWithError ( 'No new_status given' ) ;
	$user = trim ( $dbt->real_escape_string ( $tfc->getRequest ( 'user' , '' ) ) );
	if ( $user == '' ) dieWithError ( 'No user given' ) ;

	$sql = "UPDATE `page` SET `status`='{$new_status}',`user`='{$user}' WHERE `id`={$candidate_id}" ;
	$out['sql'] = $sql ;
	try {
		$tfc->getSQL ( $dbt , $sql ) ;
	} catch (Exception $e) {
		dieWithError ( $e->getMessage() ) ;
	}

} else {

	require_once '/data/project/magnustools/public_html/php/Widar.php' ;
	$widar = new \Widar ( 'fist' ) ;
	$widar->attempt_verification_auto_forward ( 'https://fist.toolforge.org/wd4wp' ) ;
	$widar->authorization_callback = 'https://fist.toolforge.org/wd4wp/api.php' ;
	if ( $widar->render_reponse ( true ) ) exit ( 0 ) ;
	$out['status'] = "Unknown action '$action'" ;
}


micDrop() ;

?>