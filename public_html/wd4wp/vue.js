'use strict';

let router ;
let app ;
let wd = new WikiData() ;

let subjects = {} ;

$(document).ready ( function () {

    vue_components.toolname = 'wd4wp' ;
//    vue_components.components_base_url = 'https://tools.wmflabs.org/magnustools/resources/vue/' ; // For testing; turn off to use tools-static
    Promise.all ( [
        vue_components.loadComponents ( ['wd-date','wd-link','tool-translate','tool-navbar','commons-thumbnail','widar','autodesc','typeahead-search','value-validator',
            //'vue_components/prop-value.html',
            'vue_components/main_page.html',
            //'vue_components/subject-page.html',
            //'vue_components/shex-page.html',
            ] )
    ] )
    .then ( () => {
        widar_api_url = 'https://fist.toolforge.org/wd4wp/api.php' ;

        wd_link_wd = wd ;
      const routes = [
        { path: '/', component: MainPage , props:true },
        { path: '/:wiki', component: MainPage , props:true },
        { path: '/:wiki/:initial_offset', component: MainPage , props:true },
        { path: '/:wiki/:initial_offset/:initial_p31', component: MainPage , props:true },
        //{ path: '/subject/:subject', component: SubjectPage , props:true },
        //{ path: '/subject/:subject/:q', component: SubjectPage , props:true },
        //{ path: '/shex/:e', component: ShexPage , props:true },
      ] ;
      router = new VueRouter({routes}) ;
      app = new Vue ( { router } ) .$mount('#app') ;
      //$('#help_page').attr('href',wd.page_path.replace(/\$1/,config.source_page));
    } ) ;

} ) ;
