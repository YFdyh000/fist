<?PHP
error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

ini_set('memory_limit','3500M');
set_time_limit ( 60 * 20 ) ; // Seconds

require_once ( 'php/ToolforgeCommon.php' ) ;
include_once ( 'php/common.php' ) ;
include_once ( 'php/oauth.php' ) ;
include_once ( 'php/wikiquery.php' ) ;
include_once ( 'php/wikidata.php' ) ;

$tfc = new ToolforgeCommon ( 'wdfist_query' ) ;
$wil = new WikidataItemList() ;
$dbwd = $tfc->openDB ( 'wikidata' , 'wikidata' ) ;
$dbc = $tfc->openDB ( 'commons' , 'wikimedia' ) ;
$dbt = $tfc->openDBtool ( 'wdfist_p' ) ;
$tfc->use_db_cache = false ;

$icons = [] ;

function isFilteredImage ( $i ) {
	global $icons ;
	if ( isset ( $icons[$i] ) ) return true ;
	if ( preg_match ( '/\.pdf$/i' , $i ) ) return true ;
	if ( preg_match ( '/\.svg$/i' , $i ) and !isset($_REQUEST['keep_svg']) ) return true ;
	if ( preg_match ( '/\.gif$/i' , $i ) ) return true ;
	if ( preg_match ( '/^Flag_of_/i' , $i ) ) return true ;
	if ( preg_match ( '/^Crystal_Clear_/i' , $i ) ) return true ;
	if ( preg_match ( '/^Nuvola_/i' , $i ) ) return true ;
	if ( preg_match ( '/^Kit_.+\.png/i' , $i ) ) return true ;
	if ( preg_match ( '/\bribbon.jpe{0,1}g/i' , $i ) ) return true ;
	if ( preg_match ( '/^600px_.+\.png/i' , $i ) ) return true ;
	if ( isset($_REQUEST['jpeg_only']) and !preg_match ( '/\.jpe{0,1}g$/i' , $i ) ) return true ;
	return false ;
}

function flushAndDie () {
	global $o ;
	header('Content-type: application/json; charset=utf-8');
	//header('Content-type: text/plain; charset=utf-8');
	print json_encode ( $o ) ;
	exit ( 0 ) ;
}

function getIndexedImage ( $i ) {
	global $o ;
	if ( !isset ( $o['images'][$i] ) ) $o['images'][$i] = count($o['images']) ;
	return $o['images'][$i] ;
}

function loadIconList () {
	global $icons , $testing ;
	global $dbt , $tfc ;
	$rows = explode ( "\n" , file_get_contents ( 'http://www.wikidata.org/w/index.php?title=User:Magnus_Manske/FIST_icons&action=raw' ) ) ;
	foreach ( $rows AS $row ) {
		if ( !preg_match ( '/^\*\s*(.+)\s*$/' , $row , $m ) ) continue ;
		$icons[$m[1]] = true ;
	}
	
	// Load multiple instances from ignored files
	$sql = "select file from ignore_files group by file having count(*) >= 3" ;
	$result = $tfc->getSQL ( $dbt , $sql ) ;
	while($row = $result->fetch_object()) $icons[$row->file] = true ;
}

function filterImages ( &$o ) {
	global $dbwd , $tfc ;
	if ( isset($_REQUEST['remove_used']) ) { // Remove files already used on wikidata
		$images = array() ;
		foreach ( $o['data'] AS $q => $v ) {
			foreach ( $v AS $img => $cnt ) $images[$img] = $dbwd->real_escape_string ( $img ) ;
		}
		
		$to_remove = array() ;
		$sql = "select DISTINCT il_to from imagelinks WHERE il_to IN ('" . implode("','",$images) . "') AND il_from_namespace=0" ;
		unset ( $images ) ;

		$result = $tfc->getSQL ( $dbwd , $sql ) ;
		while($row = $result->fetch_object()) $to_remove[] = $row->il_to ;

		foreach ( $o['data'] AS $q => $v ) {
			foreach ( $o['data'][$q] AS $img => $dummy ) {
				if ( in_array ( $img , $to_remove ) ) unset ( $o['data'][$q][$img] ) ;
			}
		}
	}
}



function getCommonsImagesByCategory ( &$items , &$o ) {
	global $wil , $tfc , $dbwd  ;

	$qlist = '' ;
	foreach ( $items AS $q ) {
		if ( $qlist != '' ) $qlist .= ',' ;
		$qlist .= "'Q$q'" ;
	}

	$items2 = array() ;
	$sql = "SELECT DISTINCT page_title from page WHERE page_title IN ($qlist) and page_namespace=0" ;
	$sql .= " AND EXISTS (SELECT * FROM pagelinks WHERE pl_from=page_id AND pl_namespace=120 and pl_title IN ('P373'))" ; # Has coordinates
	$result = $tfc->getSQL ( $dbwd , $sql ) ;
	while($row = $result->fetch_object()) $items2[] = $row->page_title ;

	if ( count($items2) == 0 ) return ; // Nope

	$wil->loadItems ( $items2 ) ;

	$urls = array() ;
	foreach ( $items2 AS $q ) {
		if ( !$wil->hasItem($q) ) continue ; // Paranoia
		$i = $wil->getItem($q) ;
		if ( !$i->hasClaims('P373') ) continue ; // Paranoia
		$claims = $i->getClaims('P373') ;
		$c = $claims[0] ;
		$category = '' . $c->mainsnak->datavalue->value ;
		$url = "https://commons.wikimedia.org/w/api.php?action=query&list=categorymembers&cmnamespace=6&cmlimit=20&cmtitle=Category:".myurlencode($category)."&format=json" ;
		$urls[$q] = $url ;
	}

	$res = $tfc->getMultipleURLsInParallel ( $urls ) ;
	
	foreach ( $res AS $q => $txt ) {
		$j = json_decode ( $txt ) ;
		if ( !isset($j) or $j == null or !isset($j->query) or !isset($j->query->categorymembers) ) continue ;
		foreach ( $j->query->categorymembers AS $v ) {
			$file = preg_replace ( '/^File:/' , '' , $v->title ) ;
			$image = preg_replace ( '/^.+?:/' , '' , $file ) ;
			if ( isFilteredImage ( $image ) ) continue ;
			$q2 = preg_replace ( '/^Q/' , '' , $q ) ;
			$o['data'][$q2][str_replace(' ','_',$image)]++ ;
		}
	}
	
}

function getCommonsImagesForItemsByCoordinates ( &$items , &$o ) {
	global $wil , $tfc , $dbwd ;

	$qlist = '' ;
	foreach ( $items AS $q ) {
		if ( $qlist != '' ) $qlist .= ',' ;
		$qlist .= "'Q$q'" ;
	}

	$items2 = array() ;
	$sql = "SELECT DISTINCT page_title from page WHERE page_title IN ($qlist) and page_namespace=0" ;
	$sql .= " AND EXISTS (SELECT * FROM pagelinks WHERE pl_from=page_id AND pl_namespace=120 and pl_title IN ('P625'))" ; # Has coordinates
	$result = $tfc->getSQL ( $dbwd , $sql ) ;
	while($row = $result->fetch_object()) $items2[] = $row->page_title ;

	if ( count($items2) == 0 ) return ; // Nope

	$wil->loadItems ( $items2 ) ; // TODO just use the geo_tags table?
	
	$radius = 100 ; // Search radius [m]
	
	$urls = array() ;
	foreach ( $items2 AS $q ) {
		if ( !$wil->hasItem($q) ) continue ; // Paranoia
		$i = $wil->getItem($q) ;
		if ( !$i->hasClaims('P625') ) continue ; // Paranoia
		$claims = $i->getClaims('P625') ;
		$c = $claims[0] ;
		$v = $c->mainsnak->datavalue->value ;
		$lat = $v->latitude ;
		$lon = $v->longitude ;
		$url = "https://commons.wikimedia.org/w/api.php?action=query&list=geosearch&gscoord=$lat|$lon&gsradius=$radius&gslimit=50&gsnamespace=6&format=json" ;
		$urls[$q] = $url ;
	}
	
	$res = $tfc->getMultipleURLsInParallel ( $urls ) ;

	foreach ( $res AS $q => $txt ) {
		$j = json_decode ( $txt ) ;
		if ( count($j->query->geosearch) == 0 ) continue ;
		foreach ( $j->query->geosearch AS $file ) {
			$image = preg_replace ( '/^.+?:/' , '' , $file->title ) ;
			if ( isFilteredImage ( $image ) ) continue ;
			$q2 = preg_replace ( '/^Q/' , '' , $q ) ;
			$o['data'][$q2][str_replace(' ','_',$image)]++ ;
		}
	}

}


function getCommonsImagesBySearch ( &$items , &$o ) {
	global $tfc , $dbwd , $wil ;
	
	$items2 = $items ;
	shuffle($items2) ;
	$items2 = array_slice($items2,0,2000);
	$q2label = $wil->getItemLabels($items,"en");
	$o['q2label'] = $q2label ;
	$urls = [] ;
	foreach ($q2label AS $q => $label ) {
		$url = "https://commons.wikimedia.org/w/api.php?action=query&list=search&srnamespace=6&format=json&srsearch=" . urlencode($row->term_text) ;
		$urls["$q"] = $url ;
	}
	$res = $tfc->getMultipleURLsInParallel ( $urls ) ;

	foreach ( $res AS $q => $txt ) {
		$j = json_decode ( $txt ) ;
		foreach ( $j->query->search AS $hit ) {
			$image = preg_replace ( '/^.+?:/' , '' , $hit->title ) ;
			if ( isFilteredImage ( $image ) ) continue ;
			$o['data'][$q][str_replace(' ','_',$image)]++ ;
		}
	}
	
}

function getCommonsImagesByLanguageLinks ( &$items , &$o ) {
	global $dbwd , $dbc , $tfc ;

	if ( !isset($o['data']) ) $o['data'] = [] ;

	// Now get the matching language links
	$ll = [] ;
	$o['q_ll'] = [] ;
	$sql = "SELECT ips_item_id AS q,ips_site_id AS site,ips_site_page AS page FROM wb_items_per_site where ips_item_id IN (" . join(',',$items) . ")" ;
	unset ( $items ) ; // Save RAM
	$result = $tfc->getSQL ( $dbwd , $sql ) ;
	while($row = $result->fetch_object()){
		if ( $row->site == 'kbpwiki' ) continue ;
		$page = str_replace(' ','_',$row->page) ;
		$ll[$row->site][$page] = $row->q ;
		if ( !isset($o['q_ll'][$row->q]) ) $o['q_ll'][$row->q] = '' ;
		$o['q_ll'][$row->q] .= '|' . urlencode($row->site.':'.$page) ;
	}

	$page_images_only = isset($_REQUEST['page_image_only']) ;
	$page_images = [] ;
	if ( $page_images_only ) {
		foreach ( $ll AS $lang => $page2q ) {
			$sql = [] ;
			foreach ( $page2q AS $page => $q ) $sql[] = "'" . $dbwd->real_escape_string($page) . "'" ;
			$sql = "SELECT DISTINCT pp_value FROM page,page_props WHERE page_id=pp_page AND page_namespace=0 AND pp_propname='page_image_free' AND page_title IN (" . join(',',$sql) . ")" ;
			$db_new = $tfc->openDBwiki ( $lang , true ) ;
			if ( $db_new === false ) { # Necessary?
				$o['nodb'][$lang]++ ;
				continue ;
			}
			$result = $tfc->getSQL ( $db_new , $sql ) ;
			while($row = $result->fetch_object()){
				$page_images[$row->pp_value] = 1 ;
			}
			$db_new->close() ;
		}
	}

	// Now get Commons images used on those
	foreach ( $ll AS $lang => $page2q ) {
		$sql = array() ;
		foreach ( $page2q AS $page => $q ) $sql[] = "'" . $dbwd->real_escape_string($page) . "'" ;
		$sql = "SELECT DISTINCT gil_page_title AS page,gil_to AS image FROM page,globalimagelinks WHERE gil_wiki='$lang' AND gil_page_namespace_id=0 AND gil_page_title IN (" . join(',',$sql) . ") AND page_namespace=6 and page_title=gil_to AND page_is_redirect=0" ;
		$sql .= " AND NOT EXISTS (SELECT * FROM categorylinks where page_id=cl_from and cl_to='Crop_for_Wikidata')" ; # To-be-cropped
		$result = $tfc->getSQL ( $dbc , $sql ) ;
		while($row = $result->fetch_object()){
			if ( isFilteredImage ( $row->image ) ) continue ;
			if ( $page_images_only and !isset($page_images[$row->image]) ) continue ;
			$q = $page2q[$row->page] ;
			$image_name = str_replace(' ','_',$row->image) ;
			if ( !isset($o['data'][$q]) ) $o['data'][$q] = [] ;
			if ( !isset($o['data'][$q][$image_name]) ) $o['data'][$q][$image_name] = 1 ;
			else $o['data'][$q][$image_name]++ ;
		}
	}
}

function filterIgnoredImages ( &$o ) {
	global $tfc , $dbt ;

	$qs = array_keys ( $o['data'] ) ;
	if ( count($qs) == 0 ) return ;

	$sql = "SELECT * FROM ignore_files WHERE q IN (" . implode(',',$qs) . ")" ;
	$result = $tfc->getSQL ( $dbt , $sql ) ;
	while($re = $result->fetch_object()){
		if ( isset($o['data'][$re->q][$re->file]) ) $o['data'][$re->q][$re->file] = 0 ;
	}
}

function filterMultipleImages ( &$o ) {
	if ( !isset ($_REQUEST['remove_multiple']) ) return ;
	
	$max_allowed_total = 5 ;
	$over_counter = array() ;
	foreach ( $o['data'] AS $q => $v ) {
		foreach ( $v AS $img => $cnt ) $over_counter[$img]++ ;
	}

	foreach ( $o['data'] AS $q => $v ) {
		foreach ( $v AS $img => $cnt ) {
			if ( $over_counter[$img] > $max_allowed_total ) unset ( $o['data'][$q][$img] ) ;
		}
	}
}

function cleanupFileList ( &$o ) {
	foreach ( $o['data'] AS $q => $images ) {
		foreach ( $images AS $image => $cnt ) {
			if ( $cnt == 0 ) unset ( $o['data'][$q][$image] ) ;
		}
		if ( count($o['data'][$q]) == 0 ) unset($o['data'][$q]) ;
	}
}

function filterItemsNoImage ( &$items ) {
	global $dbwd , $tfc , $no_images_only ;
	if ( !$no_images_only ) return ;
	
	$qlist = '' ;
	foreach ( $items AS $q ) {
		if ( $qlist != '' ) $qlist .= ',' ;
		$qlist .= "'Q$q'" ;
	}
	if ( $qlist == '' ) return ;

	$items = array() ;
	$sql = "SELECT DISTINCT page_title from page WHERE page_title IN ($qlist) and page_namespace=0 AND NOT EXISTS (SELECT * FROM pagelinks WHERE pl_from=page_id AND pl_namespace=120 and pl_title IN ('P18'))" ;
	$result = $tfc->getSQL ( $dbwd , $sql ) ;
	while($row = $result->fetch_object()){
		$items[] = preg_replace ( '/\D/' , '' , $row->page_title ) ;
	}
}

function filterListItems ( &$items ) {
	global $dbwd , $tfc ;
	$qlist = '' ;
	foreach ( $items AS $q ) {
		if ( $qlist != '' ) $qlist .= ',' ;
		$qlist .= "'Q$q'" ;
	}
	if ( $qlist == '' ) return ;

	$items = array() ;
	$sql = "SELECT DISTINCT page_title from page WHERE page_title IN ($qlist) and page_namespace=0 AND NOT EXISTS (SELECT * FROM pagelinks WHERE pl_from=page_id AND pl_namespace=0 and pl_title IN ('Q13406463','Q4167410'))" ;
	$result = $tfc->getSQL ( $dbwd , $sql ) ;
	while($row = $result->fetch_object()){
		$items[] = preg_replace ( '/\D/' , '' , $row->page_title ) ;
	}
}

function getCommonsImagesForItems ( &$items ) {
	global $o ;
	
	filterListItems ( $items ) ;
	filterItemsNoImage ( $items ) ;

	if ( count ( $items ) == 0 ) {
		$o['status'] = "No items to process" ;
		flushAndDie() ;
	}
	
	loadIconList() ;
	
	if ( isset($_REQUEST['search_by_coordinates']) ) getCommonsImagesForItemsByCoordinates ( $items , $o ) ;
	if ( isset($_REQUEST['search_by_commons_category']) ) getCommonsImagesByCategory ( $items , $o ) ;
	if ( isset($_REQUEST['search_commons']) ) getCommonsImagesBySearch ( $items , $o ) ;
	if ( !isset($_REQUEST['no_language_links']) ) getCommonsImagesByLanguageLinks ( $items , $o ) ;

	filterIgnoredImages ( $o ) ;
	filterMultipleImages ( $o ) ;
	filterImages ( $o ) ;
	cleanupFileList ( $o ) ;
	
	$o['len'] = count ( $o['data'] ) ;
	
	foreach ( $o['data'] AS $q => $v ) {
		arsort ( $o['data'][$q] , SORT_NUMERIC ) ;
	}


}


function shuffle_assoc(&$array) {
	$keys = array_keys($array);

	shuffle($keys);

	foreach($keys as $key) {
		$new[$key] = $array[$key];
	}

	$array = $new;

	return true;
}



$o = [ 'status' => 'OK' ] ;

$action = get_request ( 'action' ) ;
$no_images_only = isset ( $_REQUEST['no_images_only'] ) ;
$testing = isset ( $_REQUEST['test'] ) ;
$tusc_user = get_request ( 'user' , '' ) ;
$tusc_password = get_request ( 'pass' , '' ) ;


if ( $action == 'existing_images' ) {

	$lang = get_request ( 'lang' ) ;
	$project = get_request ( 'project' ) ;
	$pages = json_decode ( get_request ( 'pages' , '[]' ) ) ;

	$db = $tfc->openDB ( $lang , $project ) ;
	if ( false === $db ) flushAndDie() ;
	$sql = array() ;
	foreach ( $pages AS $page ) $sql[] = "'" . $db->real_escape_string ( $page ) . "'" ;
	$sql = "SELECT DISTINCT page_title,il_to,img_name FROM page,imagelinks LEFT JOIN image ON img_name=il_to WHERE il_from=page_id AND page_namespace=0 and page_title IN (" . join(',',$sql) . ")" ;
	$result = getSQL ( $db , $sql ) ;
	while($row = $result->fetch_object()){
		$o['data'][$row->page_title][$row->il_to] = isset ( $row->img_name ) ;
	}

} else if ( $action == 'crop_file' or $action == 'set_file' ) { // Only used for wiki_candidates at the moment, so doing them in one body

	$file = $dbwd->real_escape_string ( get_request ( 'file' , '' ) ) ;
	$sql = "DELETE FROM wiki_candidates WHERE `file`='$file'" ;
	$o['sql'][] = $sql ;
	getSQL ( $dbt , $sql ) ;

} else if ( $action == 'ignore_files' ) {

	$q = preg_replace ( '/\D/' , '' , get_request ( 'q' , '' ) ) ;
	$files = explode ( '|' , get_request ( 'files' , '' ) ) ;
	
	$user = get_request ( 'user' , '' ) ;
	$dbt->set_charset("utf8") ; # ???
	foreach ( $files AS $k => $v ) {
		if ( $q == '' or $user == '' ) break ;
		$file = $dbwd->real_escape_string ( $v ) ;
		$sql = "INSERT IGNORE INTO ignore_files (q,file,user) VALUES ($q,'$file','$user')" ;
		$o['sql'][] = $sql ;
		getSQL ( $dbt , $sql ) ;
		
		$sql = "DELETE FROM wiki_candidates WHERE q=$q AND `file`='$file'" ;
		$o['sql'][] = $sql ;
		getSQL ( $dbt , $sql ) ;
	}
	

} else if ( $action == 'wiki_candidates' ) {

	$wiki_candidates = strtoupper ( trim ( get_request ( 'wiki_candidates' , '' ) ) ) ;
	if ( false === $db ) die ( "Can't connect to tool DB\n" ) ;
	$dbt->set_charset("utf8") ; # ???
	$more_than = 2 ;
	if ( preg_match ( '/wiki$/' , $wiki_candidates ) ) $more_than = 4 ;
	$sql = "SELECT * FROM wiki_candidates WHERE status='OPEN' AND `group`='" . $dbwd->real_escape_string($wiki_candidates) . "' AND wiki_count>$more_than" ;
	$result = getSQL ( $dbt , $sql ) ;
	while($row = $result->fetch_object()) {
		$o['data'][''.$row->q][''.$row->file] = $row->wiki_count ;
	}
	
	filterImages ( $o ) ;
//	filterIgnoredImages ( $o ) ;

} else if ( $action == 'psid' ) {

	$psid = get_request ( 'psid' , '0' ) * 1 ;
	$url = "https://petscan.wmflabs.org/?psid=$psid&format=json&sparse=1&output_compatability=quick-intersection&common_wiki=wikidata" ;
//	print "<pre>$url</pre>" ;
	$d = json_decode ( file_get_contents ( $url ) ) ;
	$items = array() ;
	foreach ( $d->pages AS $q ) $items[] = preg_replace ( '/\D/' , '' , $q ) ; // Convert to numeric
//	print "<pre>" ; print_r ( $d ) ; print "</pre>" ;
	unset ( $d ) ;
//	print "<pre>" ; print_r ( $items ) ; print "</pre>" ;
	getCommonsImagesForItems ( $items ) ;

} else if ( $action == 'wdq' ) {

	$o['data'] = [] ;
	$wdq = get_request ( 'q' ) ;
	$wdq .= " and noclaim[31:13406463]" ; // No list items
	$d = $wdq_internal_url."?q=".urlencode($wdq) ; // TODO add noclaim for images (,maps,logos...)
	$d = json_decode ( file_get_contents ( $d ) ) ;
	
	getCommonsImagesForItems ( $d->items ) ;

} else if ( $action == 'sparql' ) {

	$sparql = get_request ( 'sparql' ) ;
	if ( preg_match ( '/\bselect\s*\?([a-zA-Z0-9_]+)/i' , preg_replace('/ DISTINCT /i',' ',$sparql) , $m ) ) {
		$v1 = $m[1] ;
		
		$sparql = str_replace ( "\n" , ' ' , $sparql ) ;
		
		// Enforce "no images"
		$rep = " OPTIONAL { ?$v1 wdt:P18 ?wdfist_dummy_1 } FILTER(!bound(?wdfist_dummy_1)) } " ; # This seems to be faster than MINUS
		$sparql = preg_replace ( '/\}([^\}]*)$/' , $rep.' $1' , $sparql ) ;
		
		$d = (object) array ( 'items' => getSPARQLitems ( $sparql , $v1 ) ) ;
		getCommonsImagesForItems ( $d->items ) ;
	} else {
		$o['status'] = 'ERROR: cannot parse SPARQL query ' . $sparql ;
	}

} else if ( $action == 'pagepile' ) {

	require_once ( '/data/project/pagepile/public_html/pagepile.php' ) ;
	$pp = new PagePile ( get_request('pagepile',-1) ) ;
	// Assumption: pile exists
	
	if ( $pp->getWiki() != 'wikidatawiki' ) {
		$pp2 = $pp->duplicate() ;
		$pp2->toWikidata() ;
		$pp = $pp2 ;
	}
	
	$pp->each ( function ( $o , $pp ) use ( &$items ) {
		if ( $o->ns != 0 ) return ;
		if ( trim($o->page) == '' ) return ;
		$items[] = preg_replace ( '/\D/' , '' , $o->page ) ;
	} ) ;
	getCommonsImagesForItems ( $items ) ;

} else if ( $action == 'itemlist' ) {

	$itemlist = explode ( "\n" , get_request ( 'itemlist' , '' ) ) ;
	$items = array() ;
	foreach ( $itemlist AS $q ) {
		$q = preg_replace ( '/\D/' , '' , $q ) ;
		if ( $q != '' ) $items[] = $q ;
	}
	getCommonsImagesForItems ( $items ) ;
	
	
} else if ( $action == 'popular_images' ) {

	$num = get_request('number','0') * 1 ;
	$items = array() ;
	$r = rand()/getrandmax() ;
	$sql = "SELECT q,image,count FROM popular_image_candidates WHERE random>=$r ORDER BY random LIMIT $num" ;
	$o['sql'] = $sql ;
	$dbt->set_charset("utf8") ; # ???
	$result = getSQL ( $dbt , $sql ) ;
	while($row = $result->fetch_object()) {
		$o['data'][''.$row->q][''.$row->image] = $row->count ;
	}
	
	filterImages ( $o ) ;

//	getCommonsImagesForItems ( $items ) ;
	
} else if ( $action == 'page_with_links' ) {

	$lang = get_request ( 'lang' ) ;
	$project = get_request ( 'project' , 'wikipedia' ) ;
	$page = get_request ( 'page_with_links' ) ;
	$wq = new WikiQuery ( $lang , $project ) ;
	
	$o['page'] = $page ;
	$links = $wq->get_links ( $page , 0 ) ;
	
	$pages = array() ;
	foreach ( $links AS $p ) {
		$pages[] = str_replace('_',' ',$dbwd->real_escape_string($p['title']));
	}
	$sql = "select distinct ips_item_id AS q from wb_items_per_site where ips_site_id='".$dbwd->real_escape_string($lang)."wiki' and ips_site_page IN ('" . join("','",$pages) . "')" ;
	$result = getSQL ( $dbwd , $sql ) ;
	$items = array() ;
	while($row = $result->fetch_object()){
		$items[] = $row->q ;
	}
	unset ( $pages ) ;

	getCommonsImagesForItems ( $items ) ;

} else if ( $action == 'from_cat' ) {

	$lang = get_request ( 'lang' ) ;
	$project = get_request ( 'project' ) ;
	$category = get_request ( 'category' ) ;
	$depth = get_request ( 'depth' ) ;
	$db = $tfc->openDB ( $lang , $project ) ;
	if ( false === $db ) flushAndDie() ;
	$pages = getPagesInCategory ( $db , $category , $depth ) ;

	foreach ( $pages AS $k => $p ) $pages[$k] = str_replace('_',' ',$db->real_escape_string($p));
	$sql = "select distinct ips_item_id AS q from wb_items_per_site where ips_site_id='".$db->real_escape_string($lang)."wiki' and ips_site_page IN ('" . join("','",$pages) . "')" ;
//	$o['sql'] = $sql ;
	$result = getSQL ( $dbwd , $sql ) ;
	$items = [] ;
	while($row = $result->fetch_object()) $items[] = $row->q ;
	unset ( $pages ) ;

	getCommonsImagesForItems ( $items ) ;
	
		
} else if ( $action == 'wikidata' ) {

	$lang = get_request ( 'lang' ) ;

	$ll = [] ;
	$tmp = [] ;
	$p2q = [] ;
	$q2p = [] ;

	// Get Q numbers for articles
	$pages = json_decode ( get_request ( 'pages' , '[]' ) ) ;
	$sql = array() ;
	foreach ( $pages AS $page ) $sql[] = "'" . str_replace('_',' ',$dbwd->real_escape_string($page)) . "'" ;
	$sql = "select ips_item_id AS q,ips_site_page AS p from wb_items_per_site where ips_site_id='".$dbwd->real_escape_string($lang)."wiki' and ips_site_page IN (" . join(',',$sql) . ")" ;
	$result = getSQL ( $dbwd , $sql ) ;
	while($row = $result->fetch_object()){
		$p = str_replace(' ','_',$row->p) ;
		$p2q[$p] = $row->q ;
		$q2p[$row->q] = $p ;
		$tmp[$p] = array ( '|q' => 'Q'.$row->q ) ;
		$o['data'][$p]['|q'] = 'Q'.$row->q ;
	}
	
	// Now get the matching language links
	$sql = "SELECT ips_item_id AS q,ips_site_id AS l,ips_site_page AS p FROM wb_items_per_site where ips_site_id!='enwiki' and ips_item_id IN (" . join(',',$p2q) . ")" ;
	$result = getSQL ( $dbwd , $sql ) ;
	while($row = $result->fetch_object()){
		$p = str_replace(' ','_',$row->p) ;
		$l = $row->l ;
		$tmp[$q2p[$row->q]][$l] = $p ;
		$ll[$l][$p][] = $q2p[$row->q] ;
	}
	
	// Now get Commons images used on those
	foreach ( $ll AS $l => $lp ) {
		$sql = array() ;
		foreach ( $lp AS $page => $orig ) $sql[] = "'" . $dbc->real_escape_string($page) . "'" ;
		$sql = "SELECT gil_page_title AS p,gil_to AS i FROM globalimagelinks WHERE gil_wiki='$l' AND gil_page_namespace_id=0 AND gil_page_title IN (" . join(',',$sql) . ")" ;
		$result = getSQL ( $dbc , $sql ) ;
		while($row = $result->fetch_object()){
			foreach ( $lp[$row->p] AS $p ) {
				$o['data'][$p]['images'][$row->i] .= str_replace('wiki','',$l) . ':' . $row->p . '|' ;
//				$o['data'][$p][$l][$row->i]++ ;
			}
		}
	}
	
} else if ( $action == 'random' ) {

	$num = 500 ;
	$r = rand() / getrandmax() ;
	$sql = "select page_title from page WHERE page_namespace=0 " ;
	$sql .= "AND NOT EXISTS (SELECT * FROM pagelinks WHERE pl_from=page_id AND pl_namespace=120 AND pl_title='P18') " ;
	$sql .= "AND NOT EXISTS (SELECT * FROM pagelinks WHERE pl_from=page_id AND pl_namespace=0 AND pl_title IN ('Q13406463','Q17562845')) " ;
	$sql .= "AND page_random>=$r order by page_random limit $num" ;
	$result = getSQL ( $dbwd , $sql ) ;
	while($row = $result->fetch_object()) $o['data'][] = $row->page_title ;

} else {
	require_once '/data/project/magnustools/public_html/php/Widar.php' ;
	$widar = new \Widar ( 'fist' ) ;
	$widar->attempt_verification_auto_forward ( '/wdfist' ) ;
	$widar->authorization_callback = 'https://fist.toolforge.org/wdfist/query.php' ;
	if ( $widar->render_reponse(true) ) exit(0);
	$o['status'] = "ERROR" ;
	$o['msg'] = "Invalid action '$action'" ;
}

flushAndDie() ;

?>