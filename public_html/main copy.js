var fist = {

	thumbsize : 180 ,
	url_batch_size : 40 ,

	image_properties : {
		'p18' : 'image' ,
		'p109' : 'signature' ,
		'p94' : 'coat&nbsp;of&nbsp;arms' ,
		'p242' : 'map' ,
		'p154' : 'logo'
	} ,
	

	lang : '' ,
	project : '' ,
	thumbs : {} ,
	thumb_class_counter : 0 ,
	icons : {} ,
	pages : {} ,

	msg : function ( k , s ) {
		if ( s.match ( /\.\.\.$/ ) ) {
			$('#loading_'+k).removeClass('label-success').addClass('label-info') ;
		} else {
			$('#loading_'+k).removeClass('label-info').addClass('label-success') ;
		}
		$('#loading_'+k).html(s) ;
		$('#loading').show() ;
	} ,
	addmsg : function ( k , s ) { this.msg ( $('#loading_'+k).html() + " " + s ) ; } ,

	init : function ( callback ) {
		var self = this ;
		$.get ( './icons.txt?'+Math.random() , function ( d ) { // random() to avoid caching
			$.each ( d.split("\n") , function ( dummy , row ) {
				if ( row == '' ) return ;
				var t = row.split("\t") ;
				if ( undefined === self.icons[t[0]] ) self.icons[t[0]] = {} ;
				self.icons[t[0]][t[1]] = t[2] ;
			} ) ;
			if ( undefined !== callback ) callback() ;
		} ) ;
		
		$('#show_existing_images').change ( function () { $('div.existing_images').toggle() } ) ;
		$('#show_articles_with_existing_images').change ( function () { $('tr.article_with_existing_images').toggle() } ) ;
		$('#show_articles_without_candidates').change ( function () { $('tr.has_no_candidates').toggle() } ) ;
	} ,

	runTool : function () {
		var self = this ;
		self.lang = $('#language').val() ;
		self.project = $('#project').val() ;

		self.thumbs = {} ;
		self.thumb_class_counter = 0 ;
		self.pages = {} ;
		
		$('#loading .label').removeClass('label-success').addClass('label-info').html('') ;
		$('#results').html('');
	
		self.runToolCategory() ;
		return false ;
	} ,

	runToolCategory : function () {
		var self = this ;
		var cat = $('#category').val() ;
		var depth = $('#depth').val() ;
		self.msg ( 'pages' , "Getting pages in category tree..." ) ;
		$.getJSON ( '//tools.wmflabs.org/catscan2/quick_intersection.php?callback=?' , {
			lang:self.lang,
			project:self.project,
			cats:cat,
			depth:depth,
			ns:0,
			start:0,
			sparse:1,
			format:'json'
		} , function ( d ) {
			self.msg ( 'pages' , "Got " + d.pagecount + " pages." ) ;
			$.each ( (d.pages||[]) , function ( dummy , page ) {
				self.pages[page] = {} ;
			} ) ;
			self.showPages () ;
			self.checkForExistingImages ( d.pages||[] ) ;
			self.checkForWikidataItems ( d.pages||[] ) ;
		} ) ;
	} ,

	getWikiThumb : function ( l , p , i , o ) {
		var self = this ;
		var pk = l + '.' + p ;
		if ( undefined !== self.icons[pk] ) {
			if ( undefined !== self.icons[pk][i] ) return '' ; // ICON
		}
		if ( undefined === self.thumbs[pk] ) self.thumbs[pk] = {} ;
		if ( undefined === self.thumbs[pk][i] ) {
			self.thumbs[pk][i] = { loaded:false , classname:'thumbclass'+self.thumb_class_counter } ;
			self.thumb_class_counter++ ;
		}
		if ( self.thumbs[pk][i].custom === undefined ) self.thumbs[pk][i].custom = o ;

		var h = "<li class='" ;
		if ( l == 'commons' ) h += 'wiki_commons' ;
		else h += 'wiki_local' ;
		h += "'>" ;

		h += "<div class='mythumb " + self.thumbs[pk][i].classname + "'>" ;

		h += "<div class='thumbtools'>" ;
		h += "<button class='btn btn-danger' onclick='fist.isAnIcon(\""+escape(pk)+"\",\""+escape(i)+"\")'>Block image</button>" ;
		h += "</div>" ;

		h += "<a target='_blank' href='//" + l + "." + p + ".org/wiki/File:" + escape(i) + "'>" ;
		h += "<img border=0 " ;
		if ( self.thumbs[pk][i].loaded ) {
			var turl = self.thumbs[pk][i].meta.thumburl ;
			var top = Math.floor((self.thumbsize-self.thumbs[pk][i].meta.thumbheight)/2) + "px" ;
			h += "class='" + self.thumbs[pk][i].classname + "' src='" + turl + "' style='margin-top:" + top + "' " ;
		}
		h += "/>" ;
		h += "</a></div>" ;
		h += "<br/><div class='thumblabel'>" ;
		h += "<a target='_blank' href='//" + l + "." + p + ".org/wiki/File:" + escape(i) + "'>" ;
		h += i.replace(/_/g,' ') ;
		h += "</a></div>" ;


		h += "</li>" ;
		return h ;
	} ,
	
	isAnIcon : function ( pk , i ) {
		if ( !tusc.logged_in ) {
			alert ( "You need to be logged in with TUSC to use this function." ) ;
			return ;
		}
		
		if ( !confirm("ATTENTION! Permanently mark "+i+" as a icon, flag, or often-used symbol image, and hide it from all users in all searches henceforth?") ) return ;
		
		var self = this ;
		pk = unescape ( pk ) ;
		i = unescape ( i ) ;
		var v = self.thumbs[pk][i] ;
		$('.'+v.classname).parents('li').remove() ;
		
		$.post ( './query.php' , {
			action:'is_an_icon',
			user:tusc.user,
			pass:tusc.pass,
			pk:pk,
			image:i
		} , function ( d ) {
			if ( d.status != 'OK' ) alert ( d.status+" : "+d.msg ) ;
		} , 'json' ) ;
		
		delete self.thumbs[pk][i] ;
	} ,

	setThumbnailImage : function ( image ) {
		var self = this ;
		if ( image.failed ) {
			$('.'+image.classname+' img').parents('a').replaceWith ( "Image does not exist." ) ;
			return ;
		}
		var turl = image.meta.thumburl ;
		var top = Math.floor((self.thumbsize-image.meta.thumbheight)/2) + "px" ;
		$('.'+image.classname+' img').attr('src',turl).css('margin-top',top) ;
	} ,


	showPages : function () {
		var self = this ;
		var pages = [] ;
		$.each ( self.pages , function ( page , v ) { pages.push ( page ) ; } ) ;
		pages = pages.sort() ;
		var h = '' ;
		h += "<table class='table table-condensed table-striped'>" ;
		h += "<tbody>" ;
		$.each ( pages , function ( row , page ) {
			self.pages[page].id = 'pagerow_' + row ;
			h += "<tr id='" + self.pages[page].id + "'>" ;
			h += "<th class='span2'>" ;
			h += "<div><a target='_blank' href='//"+self.lang+"."+self.project+".org/wiki/"+escape(page)+"'>" + page.replace(/_/g,' ') + "</a></div>" ;
			h += "<div class='wikidataitem'></div>" ;
			h += "</th>" ;
			h += "<td>" ;
			h += "<div class='existing_images'></div>" ;
			h += "<div class='candidate_images_commons'></div>" ;
			h += "</td></tr>" ;
		} ) ;
		h += "</tbody></table>" ;
		$('#results').html(h).show() ;
	} ,

	checkForExistingImages : function ( pages ) {
		var self = this ;
		if ( pages.length == 0 ) return self.msg ( 'existing' , "No pages to check for images. Done." ) ;
		self.msg ( 'existing' , "Checking for existing images..." ) ;
		$.post ( "./query.php" , {
			lang:self.lang,
			project:self.project,
			pages:JSON.stringify(pages),
			action:'existing_images'
		} , function ( d ) {
	
			if ( d.status != 'OK' ) return self.msg ( 'existing' , d.status + ' : ' + d.msg ) ;
			
			self.msg ( 'existing' , "Existing image data loaded." ) ;
		
			$.each ( d.data , function ( page , images ) {
				self.pages[page].existing_images = 0 ;
				var h = "<div class='list_label'><span class='label label-inverse'>Images that are already used in the article</span></div>";
				h += "<ul class='thumbrow'>" ;
				$.each ( images , function ( image , is_local ) {
					var h2 ;
					if ( is_local ) h2 = self.getWikiThumb ( self.lang , self.project , image ) ;
					else h2 = self.getWikiThumb ( 'commons' , 'wikimedia' , image ) ;
					if ( h2 == '' ) return ; // On the shitlist
					h += h2 ;
					self.pages[page].existing_images++ ;
				} ) ;
				h += "</ul>" ;
				if ( self.pages[page].existing_images > 0 ) $('#'+self.pages[page].id).addClass ( 'article_with_existing_images' ) ;
				else h = '' ;
				$('#'+self.pages[page].id+' div.existing_images').html(h) ;
			} ) ;
	
			if ( $('#show_existing_images').is(':checked') ) $('div.existing_images').show() ;
			if ( $('#show_articles_with_existing_images').is(':checked') ) $('tr.article_with_existing_images').show() ;
			else  $('tr.article_with_existing_images').hide() ;
			
			$('div.existing_images ul.thumbrow li').hover ( function () {
				$($(this).find('div.thumbtools')).show() ;
			} , function () {
				$($(this).find('div.thumbtools')).hide() ;
			} ) ;
			
			self.updateThumbnails() ;
	
		} , 'json' ) ;
	} ,

	updateThumbnails : function () {
		var self = this ;
		var again = true ;
	
		while ( again ) {
			again = false ;
			$.each ( self.thumbs , function ( pk , images ) {
		
				if ( pk == 'flickr' ) {

				} else { // Wikipedias; fallback

					var load = {} ;
					var files = [] ;
					$.each ( images , function ( image , v ) {
						if ( v.loaded || v.loading ) return ;
						if ( files.length >= self.url_batch_size ) { again = true ; return false ; }
						var fn = 'File:'+image.replace(/_/g,' ') ;
						files.push ( fn ) ;
						load[fn] = v ;
						v.loading = true ;
					} ) ;
			
					if ( files.length == 0 ) return ;

					$.getJSON ( '//'+pk+'.org/w/api.php?callback=?' , {
						action:'query',
						titles:files.join('|'),
						prop:'imageinfo',
						iiprop:'url',
						iiurlwidth:self.thumbsize,
						iiurlheight:self.thumbsize,
						redirects:'',
						format:'json'
					} , function ( d ) {
						var redir = {} ;
						$.each ( (d.query.redirects||[]) , function ( dummy , v ) { redir[v.to] = v.from ; } ) ;
						$.each ( (d.query.pages||[]) , function ( id , image ) {
							if ( id < 0 ) return ;
							if ( undefined === image.imageinfo ) return ;
							var t = image.title ;
							if ( undefined === load[t] ) {
								if ( undefined === load[redir[t]] ) { console.log ( image.title ) ; return ; }
								t = redir[t] ;
							}
							load[t].title = image.title ;
							load[t].loading = false ;
							load[t].loaded = true ;
							load[t].meta = image.imageinfo[0] ;
							self.setThumbnailImage ( load[t] ) ;
						} ) ;
						
						$.each ( load , function ( image , v ) {
							if ( v.loaded ) return ;
							v.loading = false ;
							v.failed = true ;
							self.setThumbnailImage ( v ) ;
						} ) ;
				
					} ) ;

				}
			} ) ;
		
		}
	} ,
	
	checkForWikidataItems : function ( pages ) {
		var self = this ;
		self.msg ( 'wikidata' , "Loading Wikidata and Commons image usage (may take a while)..." ) ;
		$.post ( './query.php' , {
			action:'wikidata',
			lang:self.lang,
			project:self.project,
			pages:JSON.stringify(pages)
		} , function ( d ) {
			if ( d.status != 'OK' ) return self.msg ( 'wikidata' , d.status + ' : ' + d.msg ) ;
			self.msg ( 'wikidata' , "Wikidata and Commons usage in other languages loaded." ) ;
			
			$.each ( d.data , function ( page , v ) {
			
				// Wikidata item
				self.pages[page].q = v['|q'] ;
				var h = "<small><div>WD:&nbsp;<a target='_blank' href='//www.wikidata.org/wiki/" + self.pages[page].q + "'>" + self.pages[page].q + "</a></div>" ;
				h += "<div class='wikidata_images'></div></small>" ;
				$('#'+self.pages[page].id+' div.wikidataitem').attr('id','item_'+self.pages[page].q).html(h).show() ;

				// Commons candidates
				self.pages[page].candidates = 0 ;
				var h = "<div class='list_label'><span class='label label-inverse'>Candidate images from Commons used in other languages</span></div>";
				h += "<ul class='thumbrow'>" ;
				$.each ( (v.images||[]) , function ( image , projects ) {
					var o = { projects:projects.split(/\|/g) } ;
					var h2 = self.getWikiThumb ( 'commons' , 'wikimedia' , image , o ) ;
					if ( h2 == '' ) return ;
					h += h2 ;
					self.pages[page].candidates++ ;
				} ) ;
				h += "</ul>" ;
				if ( self.pages[page].candidates == 0 ) {
					$('#'+self.pages[page].id).addClass ( 'has_no_candidates' ) ;
					h = '' ;
				} else $('#'+self.pages[page].id).removeClass ( 'has_no_candidates' ) ;
				$('#'+self.pages[page].id+' div.candidate_images_commons').html(h) ;
			} ) ;
			
			if ( $('#show_articles_without_candidates').is(':checked') ) $('tr.has_no_candidates').show() ;
			else $('tr.has_no_candidates').hide() ;
			
			$('div.candidate_images_commons').show() ;

			$('div.candidate_images_commons ul.thumbrow li').hover ( function () {
				$($(this).find('div.thumbtools')).show() ;
			} , function () {
				$($(this).find('div.thumbtools')).hide() ;
			} ) ;

			self.updateThumbnails () ;
			
			var wd = new WikiData () ;
			var qlist = [] ;
			$.each ( self.pages , function ( k , v ) { if ( v.q !== undefined ) qlist.push ( v.q ) } ) ;
			wd.loadItems ( qlist , { finished : function ( p ) {
				
				$.each ( self.pages , function ( page , v ) {
					if ( v.q === undefined ) return ;
					if ( undefined === wd.items[v.q.toLowerCase()] ) return ;
					var item = wd.items[v.q.toLowerCase()] ;
					$.each ( self.image_properties , function ( p , s ) {
						var x = item.getClaimsForProperty ( p ) ;
						if ( 0 == x.length ) return ;
						$.each ( x , function ( dummy , v2 ) {
							var i = v2.mainsnak.datavalue.value ;
							var url = "//commons.wikimedia.org/wiki/File:" + escape ( i ) ;
							var h = "<div><a target='_blank' href='" + url + "'>" + s + "</a></div>" ;
							$('#item_'+v.q+' div.wikidata_images').append ( h ) ;
						} ) ;
					} ) ;
				} ) ;
				
			} , follow:[] , preload:[] } , 0 ) ;
			
			
		} , 'json' ) ;
	} ,

	fin : ''
} ;


$(document).ready ( function () {
	loadMenuBarAndContent ( { toolname : 'FIST' , meta : 'FIST' , content : 'form.html' , run : function () {
		tusc.setupLoginBar ( $('#tusc_container_wrapper') , function () {
			wikiDataCache.ensureSiteInfo ( [ { lang:'commons' , project:'wikimedia' } ] , function () {
	
				$('#toolname').html ( "<b>F</b>ree <b>I</b>mage <b>S</b>earch <b>T</b>ool" ) ;
				tusc.initializeTUSC () ;
				tusc.addTUSC2toolbar() ;
				$('#category').focus() ;
				
				fist.init ( function () {

					// TESTING FIXME
					$('#category').val ( 'German biologists' ) ;
					$('#depth').val ( 0 ) ;
					fist.runTool() ;
					
				} ) ;
				
			} ) ;
		} ) ;
	} } ) ;
} ) ;
