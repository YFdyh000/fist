#!/usr/bin/php
<?PHP
error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','1800M');

include_once ( '/data/project/fist/public_html/php/common.php' ) ;

$min_wikis_supporting = 2 ;

function updateWiki ( $wiki ) {
	
	$dbw = openDBwiki ( $wiki ) ;
	if ( false === $dbw ) die ( "Can't connect to $wiki DB\n" ) ;

	$sql = 'SELECT  page_title,p1.pp_value AS q,p2.pp_value AS image FROM page,page_props p1,page_props p2 ' ;
	$sql .= 'WHERE page_is_redirect=0 AND page_namespace=0 ' ;
	$sql .= 'AND page_id=p1.pp_page AND p1.pp_propname="wikibase_item" AND page_id=p2.pp_page AND p2.pp_propname="page_image" ' ;
	$sql .= 'AND NOT EXISTS (SELECT * FROM wikidatawiki_p.page p0,wikidatawiki_p.pagelinks WHERE p0.page_title=p1.pp_value AND p0.page_namespace=0 AND p0.page_is_redirect=0 AND pl_from=p0.page_id AND ' ;
	$sql .= '( (pl_title IN ("Q4167410","Q13406463","Q14204246","Q3146899") AND pl_namespace=0) OR (pl_title="P18" AND pl_namespace=120) )' ;
	$sql .= ') AND NOT EXISTS (SELECT * FROM image WHERE img_name=p2.pp_value)' ;
	if(!$result = $dbw->query($sql)) die('1: There was an error running the query [' . $dbw->error . ']'."\n$sql\n");

	$dbt = openToolDB ( 'wdfist_p' ) ;
	if ( false === $dbt ) die ( "Can't connect to tool DB\n" ) ;
	$dbt->set_charset("utf8") ;

	while($o = $result->fetch_object()){
		$q = preg_replace ( '/\D/' , '' , $o->q ) ;
		$image = $dbt->real_escape_string ( $o->image ) ;
		$sql = "INSERT IGNORE INTO by_wiki (wiki,q,image) VALUES ('$wiki'," . $q . ",'" . $image . "')" ;
		if(!$result2 = $dbt->query($sql)) die('3: There was an error running the query [' . $dbt->error . ']'."\n$sql\n");
	}
	
	# Get often-used images (for more than two articles on this wiki)
	$images = array() ;
	$sql = "SELECT DISTINCT image FROM by_wiki WHERE wiki='$wiki' group by image HAVING count(distinct q)>2" ;
	if(!$result = $dbt->query($sql)) die('5a: There was an error running the query [' . $dbt->error . ']'."\n$sql\n");
	while($o = $result->fetch_object()) $images[] = $dbt->real_escape_string ( $o->image ) ;
	
	// Remove often-used images
	if ( count($images) == 0 ) return ; // Done
	$sql = "DELETE FROM by_wiki WHERE wiki='$wiki' AND image IN ('" . implode ( "','" , $images ) . "')" ;
	if(!$result = $dbt->query($sql)) die('5b: There was an error running the query [' . $dbt->error . ']'."\n$sql\n");
}

$languages = array ( 'de','pl','en','fr','sv','no','it','es','pt','zh','jp' ) ;
$wikis = array() ;
foreach ( $languages AS $l ) $wikis[] = $l.'wiki' ;

// Clear detail table
$sql = "TRUNCATE by_wiki" ;
$dbt = openToolDB ( 'wdfist_p' ) ;
if ( false === $dbt ) die ( "Can't connect to tool DB\n" ) ;
$dbt->set_charset("utf8") ;
if(!$result = $dbt->query($sql)) die('4: There was an error running the query [' . $dbt->error . ']'."\n$sql\n");

// Get Commons images per item, for each wiki
foreach ( $wikis AS $wiki ) updateWiki ( $wiki ) ;

// Generate summary table
$dbt = openToolDB ( 'wdfist_p' ) ;
if ( false === $dbt ) die ( "Can't connect to tool DB\n" ) ;
$dbt->set_charset("utf8") ;

$sql = "TRUNCATE popular_image_candidates" ;
if(!$result = $dbt->query($sql)) die('6: There was an error running the query [' . $dbt->error . ']'."\n$sql\n");
$sql = "INSERT INTO popular_image_candidates (q,image,count,random) SELECT q,image,count(*) AS cnt,rand() FROM by_wiki WHERE image NOT LIKE '%.svg' AND image NOT LIKE '%.png'  AND image NOT LIKE '%.gif' AND NOT EXISTS (SELECT * FROM ignore_files WHERE file=image) GROUP BY q,image HAVING cnt>=$min_wikis_supporting" ;
if(!$result = $dbt->query($sql)) die('7: There was an error running the query [' . $dbt->error . ']'."\n$sql\n");

?>