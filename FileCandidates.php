<?PHP

require_once ( "/data/project/fist/public_html/php/common.php" ) ;
require_once ( "/data/project/fist/public_html/php/ToolforgeCommon.php" ) ;
require_once ( "/data/project/fist/public_html/php/wikidata.php" ) ;

class FileCandidates {
	public $dbt ; // WDFIST tool database handle
	public $dbw ; // Wikidata database handle
	public $wil ; // Wikidata Item List
	public $tfc ;

	private $sparql_cache = [] ;
	private $flickr_api_key ;
	public $flickr_total_pages ; # For last query

	public $groups2property = [
		'TAXON AUDIO' => 'P51'
	] ;

	function __construct() {
		$this->dbt = openToolDB ( 'wdfist_p' ) ;
		$this->dbw = openDB ( 'wikidata' , 'wikidata' ) ;
		$this->wil = new WikidataItemList ;
		$this->tfc = new ToolforgeCommon ( 'FileCandidates' ) ;
	}

	public function rand() {
		return rand()/getrandmax() ;
	}

	public function escape ( $s ) {
		return $this->dbw->real_escape_string ( $s ) ;
	}

	public function addFile ( $params ) {
		# Paranoia to code around https://phabricator.wikimedia.org/T253373
		if ( $params['source'] == 'COMMONS' ) {
			if ( is_array($params['json']) and !isset($params['json']['imageinfo']) ) return ;
			if ( is_object($params['json']) and !isset($params['json']->imageinfo) ) return ;
		}

		$keys = [ 'random' ] ;
		$values = [ 'rand()' ] ;

		// Try adding some more info
		if ( !isset($params['source']) ) $params['source'] = 'COMMONS' ;
		if ( !isset($params['file_type']) and is_object($params['json']) ) {
			if ( $params['source'] == 'COMMONS' ) $params['file_type'] = $this->getFileTypeByExtension ( $params['json']->title ) ;
			else if ( $params['source'] == 'FLICKR' ) $params['file_type'] = 'IMAGE' ;
		}

#		if ( isset($params['file_id']) and $params['file_id']!='' and $this->doesFileCandidateExists ( $params['source'] , $params['file_id'] ) ) return ;

		foreach ( $params AS $k => $v ) {
			if ( $k == 'random' ) continue ;
			$keys[] = '`' . $k . '`' ;
			if ( $k == 'q' ) $v = preg_replace ( '/\D/' , '' , $v ) ;
			if ( $k == 'json' ) {
				if ( is_array($v) or is_object($v) ) $v = json_encode($v) ;
			}
			$v = $this->escape($v) ;
			if ( $k != 'q' ) $v = "'$v'" ;
			$values[] = $v ;
		}
		if ( $params['source'] == 'COMMONS' and $params['file_id'] == -1 ) return ; // Paranoia
		$sql = "INSERT IGNORE INTO file_candidates (" . implode(',',$keys) . ") VALUES (" . implode(',',$values) . ")" ;
	 	$this->tfc->getSQL ( $this->dbt , $sql , 2 ) ;
		if ( $this->dbt->affected_rows == 1 ) {
			$this->logFileEvent ( $this->dbt->insert_id , 'ADDED' , isset($params['user'])?'USER':'SYSTEM' ) ;
		}
	}

	public function logFileEvent ( $file_id , $event , $actor ) {
		$ts = $this->getTimestamp() ;
		$file_id *= 1 ;
		$event = $this->escape($event) ;
		$actor = $this->escape($actor) ;
		$sql = "INSERT INTO fc_log (fc_id,fc_event,fc_actor,fc_timestamp) VALUES ({$file_id},'{$event}','{$actor}','$ts')" ;
		$this->tfc->getSQL ( $this->dbt , $sql , 2 ) ;
	}

	public function doesFileCandidateExists ( $source , $file_id ) { # NOT file_candidates.id, but file_candidates.file_id !!!
		$source = $this->escape ( $source ) ;
		$file_id = $this->escape ( $file_id ) ;
		$sql = "SELECT * FROM file_candidates WHERE `source`='{$source}' AND file_id='{$file_id}' LIMIT 1" ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql , 2 ) ;
		if ($o = $result->fetch_object()) return true ;
		return false ;
	}

	public function getRandomItems ( $count , $params=[] , $no_random = false ) {
		global $out ;
		$ret = [] ;
		$tries_left = $no_random ? 1 : 10 ;
		while ( $tries_left > 0 and count($ret) < $count ) {
			$r = $this->rand() ;
			if ( $no_random ) $r = 0 ;
			$sql = "SELECT * FROM file_candidates WHERE random>=$r HAVING " . $this->getConditionsFromParameters($params) . " ORDER BY random LIMIT " . ($count*2) ;
#			if ( isset($out) ) $out['sql'][] = "getRandomItems: {$sql}" ; # DEBUGGING
			$result = $this->tfc->getSQL ( $this->dbt , $sql ) ;
			while ( $o = $result->fetch_object() and count($ret) < $count ) $ret[] = $o->q ;
			$tries_left-- ;
		}
		$this->addPrefixToItemList ( $ret ) ;
		return $ret ;
	}

	public function getRandomItemCandidates ( $count , $params=[] ) {
		$ret = [] ;
		$r = $this->rand() ;
		$sql = "SELECT * FROM file_candidates WHERE random>=$r AND " . $this->getConditionsFromParameters($params) . " ORDER BY random LIMIT " . ($count*1) ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql , 2 ) ;
		while($o = $result->fetch_object()) {
			$o->json = json_decode($o->json) ;
			$ret[] = $o ;
		}
		return $ret ;
	}

	public function getFileCandidatesForItems ( $items , $params=[] ) {
		global $out ;
		$ret = [] ;
		$this->removePrefixFromItemList ( $items ) ;
		if ( count($items) == 0 ) return $ret ;
		$sql = "SELECT * FROM file_candidates WHERE q IN (" . implode(',',$items) . ") AND " . $this->getConditionsFromParameters($params) ;
#		if ( isset($out) ) $out['sql'][] = "getRandomItems: {$sql}" ; # DEBUGGING
		$result = $this->tfc->getSQL ( $this->dbt , $sql , 2 ) ;
		while($o = $result->fetch_object()) {
			$o->json = json_decode($o->json) ;
			$ret['Q'.$o->q][] = $o ;
		}
		foreach ( $ret AS $q => $files ) {
			shuffle ( $ret[$q] ) ;
		}
		return $ret ;
	}

	private function getConditionsFromParameters ( $params=[] ) {
		$conditions = [] ;
		foreach ( array('source','group','status','file_type') AS $key ) {
			if ( !isset($params[$key]) ) continue ;
			if ( $params[$key] == '' ) continue ;

			if ( is_array($params[$key]) ) {
				$tmp = [] ;
				foreach ( $params[$key] AS $v ) {
					if ( $v != '' ) $tmp[] = '"'  . $this->escape($v) . '"' ;
				}
				if ( count($tmp) == 0 ) continue ;
				$conditions[] = '(`' . $key . '` IN (' . implode(',',$tmp) . '))' ;
			} else {
				if ( $params[$key] == '' ) continue ;
				$value = '"'  . $this->escape($params[$key]) . '"' ;
				$conditions[] = '(`' . $key . "`=$value)" ;
			}
		}

		if ( isset($params['q_zero']) ) {
			$conditions[] = "(q=0)" ;
		} else {
			$conditions[] = "(q!=0)" ;
		}

		if ( isset($params['sparql']) and trim($params['sparql'])!='' ) {
			$sparql = trim($params['sparql']) ;
			if (!isset($this->sparql_cache[$sparql])) {
				$tmp = $this->tfc->getSPARQLitems ( $sparql ) ;
				$tmp = implode ( ',' , $tmp ) ;
				$tmp = preg_replace ( '/[^0-9,]/' , '' , $tmp ) ; // Remove 'Q'
				$this->sparql_cache[$sparql] = $tmp ;
			}
			if ( $this->sparql_cache[$sparql] != '' ) {
				$conditions[] = '(q IN (' . $this->sparql_cache[$sparql] . '))' ;
			}
		}

		if ( isset($params['commonscat']) and trim($params['commonscat'])!='' ) {
			$category = $this->escape ( str_replace ( ' ' , '_' , $params['commonscat'] ) ) ;
			$page_ids = [] ;
			$sql = "SELECT DISTINCT cl_from FROM commonswiki_p.categorylinks WHERE cl_to='{$category}'" ;
			$result = $this->tfc->getSQL ( $this->dbw , $sql ) ;
			while($o = $result->fetch_object()) $page_ids[] = $o->cl_from ;
			if ( count($page_ids) > 0 ) {
				$cond = "(`source`!='COMMONS' OR `file_id` IN (" . implode(',',$page_ids) . "))" ;
				$conditions[] = $cond ;
			} else {
				global $out ;
				if ( isset($out) ) $out['warning'] = 'No images candidates found for this category, using all images' ;
			}
		}

		//print "<pre>" ; print_r ( $conditions ) ; print "</pre>" ;
		if ( count($conditions) == 0 ) return '(1)' ;
		$ret = '(' . implode ( ' AND ' , $conditions ) . ')' ;
		return $ret ;
	}

	public function getTimestamp () {
		return date ( 'YmdHis' ) ;
	}

	public function getFileCandidateCount ( $params=[] ) {
		$total = 0 ;
		$sql = "SELECT count(*) AS cnt FROM file_candidates WHERE " . $this->getConditionsFromParameters($params) ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql , 2 ) ;
		while($o = $result->fetch_object()) $total = $o->cnt ;
		return $total ;
	}

	public function getItemCandidateCount ( $params=[] ) {
		$total = 0 ;
		$sql = "SELECT count(DISTINCT q) AS cnt FROM file_candidates WHERE " . $this->getConditionsFromParameters($params) ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql , 2 ) ;
		while($o = $result->fetch_object()) $total = $o->cnt ;
		return $total ;
	}

	public function addPrefixToItemList ( &$qs ) {
		foreach ( $qs AS $k => $v ) $qs[$k] = 'Q' . preg_replace ( '/\D/' , '' , "$v" ) ;
	}


	public function removePrefixFromItemList ( &$qs ) {
		foreach ( $qs AS $k => $v ) $qs[$k] = preg_replace ( '/\D/' , '' , "$v" ) ;
	}

	public function doesItemHaveImage ( $q , $prop = 'P18' ) {
		$q = 'Q' . preg_replace ( '/\D/' , '' , "$q" ) ;
		$sql = "SELECT DISTINCT page_title FROM page,pagelinks WHERE page_id=pl_from AND page_title='$q' AND page_namespace=0 AND pl_namespace=120 AND pl_title='".$this->escape($prop)."'" ;
		$result = $this->tfc->getSQL ( $this->dbw , $sql , 2 ) ;
		if($o = $result->fetch_object()) return true ;
		return false ;
	}

	// $qs is an array of items
	// Returns an array 
	public function markItemsWithImagesOnWikidataAsSkipped ( $qs ) {
		if ( count($qs) == 0 ) return ;
		$this->addPrefixToItemList ( $qs ) ;
		$sql = "SELECT DISTINCT page_title FROM page,pagelinks WHERE page_id=pl_from AND page_title IN ('" . implode("','",$qs) . "') AND page_namespace=0 AND pl_namespace=120 AND pl_title='P18'" ;
		$result = $this->tfc->getSQL ( $this->dbw , $sql , 2 ) ;
		$qs = [] ;
		while($o = $result->fetch_object()) $qs[] = preg_replace ( '/\D/' , '' , $o->page_title ) ;
		if ( count($qs) > 0 ) {
			$cond = "`status`='WAIT' AND q IN (" . implode(',',$qs) . ")" ;
			$sql = "SELECT id FROM file_candidates WHERE $cond" ;
			$result = $this->tfc->getSQL ( $this->dbt , $sql , 2 ) ;
			while($o = $result->fetch_object()) {
				$this->logFileEvent ( $o->id , 'OTHER_FILE_USED' , 'SYSTEM' ) ;
			}

			$sql = "UPDATE file_candidates SET `status`='SKIPPED' WHERE $cond" ;
			$result = $this->tfc->getSQL ( $this->dbt , $sql , 2 ) ;
		}
		return $qs ;
	}

	private function markItemsWithDoneImagesAsSkipped () {
		$sql = 'SELECT DISTINCT q FROM file_candidates WHERE `status`="DONE"' ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql , 2 ) ;
		$qs = [] ;
		while($o = $result->fetch_object()) $qs[] = $o->q ;
		if ( count($qs) > 0 ) {
			$cond = "`status`='WAIT' AND q IN (" . implode(',',$qs) . ")" ;
			$sql = "SELECT id FROM file_candidates WHERE $cond" ;
			$sql .= " AND `group` NOT IN ('" . implode ( "','" , array_keys($this->groups2property) ) . "')" ; // Don't mark e.g. audio as done if there's an image. TODO by groups
			$result = $this->tfc->getSQL ( $this->dbt , $sql , 2 ) ;
			while($o = $result->fetch_object()) {
				$this->logFileEvent ( $o->id , 'OTHER_FILE_USED' , 'SYSTEM' ) ;
			}

			$sql = "UPDATE file_candidates SET `status`='SKIPPED' WHERE $cond" ;
			$result = $this->tfc->getSQL ( $this->dbt , $sql , 2 ) ;
		}
	}

	public function getCommonsImageInfo ( $filename ) {
		$filename = $this->normalizeCommonsFilename($filename);
		$url = 'https://commons.wikimedia.org/w/api.php?action=query&titles=File:' . urlencode($filename) . '&prop=imageinfo&format=json&iiprop=url&iiurlwidth=120&iiurlheight=120' ;
		$data = file_get_contents ( $url ) ;
		if ( $data == null || $data == '' ) return ;
		$data = json_decode ( $data ) ;
		if ( !isset($data->query) ) return ;
		if ( !isset($data->query->pages) ) return ;
		if ( !is_object($data->query->pages) ) return ;
		foreach ( $data->query->pages AS $k => $v ) {
			if ( $k != -1 ) return $v ;
		}
	}

	public function normalizeCommonsFilename ( $filename ) {
		return str_replace ( '_' , ' ' , preg_replace ( '/^File:/' , '' , $filename ) ) ;
	}

	public function getFileTypeByExtension ( $filename ) { // 'IMAGE','AUDIO','VIDEO','SVG','UNKNOWN','DOCUMENT'
		if ( preg_match ( '/\.(JPG|JPEG|PNG|XCF|TIF|TIFF|GIF)$/i' , $filename ) ) return 'IMAGE' ;
		if ( preg_match ( '/\.(FLAC|OGG|MP3|MIDI|WAVE)$/i' , $filename ) ) return 'AUDIO' ;
		if ( preg_match ( '/\.(OG.|WEBM)$/i' , $filename ) ) return 'VIDEO' ;
		if ( preg_match ( '/\.(SVG)$/i' , $filename ) ) return 'SVG' ;
		if ( preg_match ( '/\.(DJVU|PDF)$/i' , $filename ) ) return 'DOCUMENT' ;
		return 'UNKNOWN' ;
	}

	public function getFlickrAPIkey () {
		if ( isset($this->flickr_api_key) ) return $this->flickr_api_key ;
		$this->flickr_api_key = trim ( file_get_contents ( '/data/project/fist/flickr_key.txt' ) ) ;
		return $this->flickr_api_key ;
	}

	private function object2array($object) {
		return @json_decode(@json_encode($object),1);
	} 


	private function runFlickrQuery ( $url ) {
		$ret = [] ;

		sleep ( 0.5 ) ;
		$data = @file_get_contents ( $url ) ;

		if ( !isset($data) or $data === null or $data == '' ) return $ret ;
		$data = simplexml_load_string ( $data ) ;
		$data = $this->object2array ( $data ) ;

		if ( !isset($data['photos']) 
			or !isset($data['photos']['@attributes']) 
			or !isset($data['photos']['@attributes']['pages']) 
			or $data['photos']['@attributes']['pages'] == 0 ) return $ret ;

		$this->flickr_total_pages = $data['photos']['@attributes']['pages'] * 1 ;

		$ret = $data['photos']['photo'] ;
		if ( is_object($ret) and isset($ret->{'@attributes'}) ) $ret = [$ret] ;
		else if ( is_array($ret) and isset($ret['@attributes']) ) $ret = [$ret] ;
		return $ret ;
	}

	private function getFlickrQueryURL ( $query , $page = 1 , $per_page = 10 ) {
		$this->getFlickrAPIkey() ;
		$url = "https://api.flickr.com/services/rest/?method=flickr.photos.search" ;
		$url .= "&api_key=" . $this->flickr_api_key ;
		$url .= "&text=" . urlencode ( $query ) ;
		$url .= "&content_type=1" ;
		$url .= "&license=4,5,7,8,9" ; # CC-BY, CC-BY-SA, CC-0, PD-US-GOV
		$url .= "&extras=license,url_t,description,geo,tags" ;
		$url .= "&per_page=$per_page" ;
		$url .= "&page=$page" ;
		return $url ;
	}

	public function searchFlickr ( $query ) {
		$url = $this->getFlickrQueryURL ( $query ) ;
		return $this->runFlickrQuery ( $url ) ;
	}

	public function searchFlickrAll ( $query , $page ) {
		$ret = [] ;
		$url = $this->getFlickrQueryURL ( $query , $page , 500 ) ;
		$files = $this->runFlickrQuery ( $url ) ;
		foreach ( $files AS $file ) {
			if ( !isset($file['@attributes']) ) continue ; // Paranoia
			$ret[] = $file ;
		}
		return $ret ;
	}

	public function searchCommons ( $query , $namespace = 6 , $limit = 10 , $sort_order = '' ) {
		$ret = [] ;
		$url = "https://commons.wikimedia.org/w/api.php?action=query&generator=search&gsrnamespace=$namespace&format=json&prop=imageinfo&iiprop=url&iiurlwidth=120&iiurlheight=120" ;
		$url .= "&gsrsearch=" . urlencode ( $query ) ;
		$url .= "&gsrlimit=" . $limit ;
		if ( $sort_order != '' ) $url .= "&gsrsort=" . urlencode($sort_order) ;
		$data = file_get_contents ( $url ) ;
		if ( $data == null || $data == '' ) return $ret ;
		$data = json_decode ( $data ) ;
		if ( !isset($data->query) ) return $ret ;
		if ( !isset($data->query->pages) ) return $ret ;
		if ( count($data->query->pages) == 0 ) return $ret ;
		return $data->query->pages ;
	}

	public function markItemsWithImagesAsSkipped () {
		// Remove bad Commons files
		$sql = "DELETE FROM file_candidates WHERE `source`='COMMONS' AND file_id=-1" ;
		$this->tfc->getSQL ( $this->dbt , $sql , 2 ) ;

		// Internal check: Items that have a "DONE" image should not have any "WAIT"
		$this->markItemsWithDoneImagesAsSkipped () ;

		// Wikidata check
		$sql = 'SELECT DISTINCT q FROM file_candidates WHERE `status`="WAIT"' ;
		$sql .= " AND `group` NOT IN ('" . implode ( "','" , array_keys($this->groups2property) ) . "')" ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql , 2 ) ;
		$qs = [] ;
		while($o = $result->fetch_object()) $qs[] = $o->q ;
		$this->markItemsWithImagesOnWikidataAsSkipped ( $qs ) ;
	}

}

?>