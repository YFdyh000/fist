#!/usr/bin/php
<?php

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( '/data/project/fist/FileCandidates.php') ;

$max_files_per_search_result = 20 ;

$fc = new FileCandidates ;

$sparql = 'SELECT ?q ?file { ?img wdt:P180 ?q ; wdt:P18 ?file . MINUS { ?q wdt:P18 [] } }' ;
$j = getSPARQL ( $sparql ) ;

foreach ( $j->results->bindings AS $b ) {
	$q = preg_replace ( '/^.+\/Q/' , 'Q' , $b->q->value ) ;
	$filename = preg_replace ( '/^.+?Special:FilePath\//' , '' , $b->file->value ) ;
	$filename = urldecode ( $filename ) ;
	if ( preg_match ( '/\.(pdf|svg|og.|mp.)$/i' , $filename ) ) continue ;

	$file = $fc->getCommonsImageInfo ( $filename ) ;
	if ( !isset($file) ) continue ;
	
	$fc->addFile ( [
		'q' => $q ,
		'json' => $file ,
		'group' => 'WD_DEPICTS' ,
		'source' => 'COMMONS' ,
		'file_id' => $file->pageid
	] ) ;
}

?>