#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_ALL);
ini_set('display_errors', 'On');

include_once ( '/data/project/fist/FileCandidates.php' ) ;
$fc = new FileCandidates ;
$dbc = openDB ( 'commons' , 'wikimedia' ) ;

$url = 'https://petscan.wmflabs.org/?psid=11242068&format=json' ;
$j = json_decode(file_get_contents($url));

$sparql_cache = [];
foreach ( $j->{'*'}[0]->a->{'*'} AS $page ) {
	# Check if already in DB
	if ( $fc->doesFileCandidateExists('COMMONS',$page->id) ) continue ;

	# Get non-hidden categories of file that look like taxon names
	$sql = "SELECT cl_to from page,categorylinks where page_namespace=6 AND page_title='".$fc->escape($page->title)."' AND page_id=cl_from AND NOT EXISTS (select * from page,page_props where cl_to=page_title AND page_namespace=14 AND pp_page=page_id AND pp_propname='hiddencat')";
	$result = getSQL ( $dbc , $sql ) ;
	$categories = [] ;
	while($o = $result->fetch_object()){
		# Make sure eack kept category could be a valid taxon name
		$category = str_replace('_',' ',$o->cl_to) ;
		if ( preg_match('/^Unidentified ([A-Z][a-z]+)$/',$category,$m) ) $category = $m[1] ;
		else if ( !preg_match('/^[A-Z][a-z]+ [a-z]+$/',$category) ) continue ;
		$categories[] = $category;
	}
	if ( count($categories) == 0 ) continue ;
	#print "{$page->title} => " . join('|',$categories) . "\n" ;

	# Check what taxa the categories correspond to
	$sparql = "SELECT DISTINCT ?q { VALUES ?name {'" . join("' '",$categories) . "'} . ?q wdt:P31 wd:Q16521 ; wdt:P225 ?name }";
	if ( isset($sparql_cache[$sparql]) ) {
		$items = $sparql_cache[$sparql];
	} else {
		$items = $fc->tfc->getSPARQLitems ( $sparql ) ;
		$sparql_cache[$sparql] = $items ;
	}
	if ( count($items) == 0 ) continue ;

	# Add candidates to DB
	$nice_page_title = $fc->normalizeCommonsFilename($page->title);
	foreach ( $items AS $q ) {
		if ( $fc->doesItemHaveImage($q,'P10') ) continue;
		$json = $fc->getCommonsImageInfo ( $page->title ) ;
		if ( !isset($json) ) continue ;
		$fc->addFile ( [
			'q' => $q ,
			'json' => $json ,
			'group' => 'TAXON_VIDEO' ,
			'source' => 'COMMONS' ,
			'file_type' => 'VIDEO' ,
			'file_id' => $json->pageid
		] ) ;
	}
}

?>