#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_ALL);
ini_set('display_errors', 'On');

include_once ( '/data/project/fist/FileCandidates.php' ) ;
$fc = new FileCandidates ;

if ( !isset($argv[2]) ) die ( "Usage: command CATEGORY GROUP (no '#')\n") ;

$category = $argv[1] ;
$group = '#' . strtoupper($argv[2]) ;

$dbc = openDB ( 'commons' , 'wikimedia' ) ;
$files = getPagesInCategory ( $dbc , $category , 5 , 6 , true ) ;



foreach ( $files AS $file ) {
	$j = $fc->getCommonsImageInfo ( $file ) ;
	if ( $j === null ) continue ; // Paranoia
	$fc->addFile ( [
		'q' => 0 ,
		'json' => $j ,
		'group' => $group ,
		'source' => 'COMMONS' ,
		'comment' => 'From category '.$category ,
		'file_id' => $j->pageid 
	] ) ;
}

?>