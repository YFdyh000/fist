#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_ALL);
ini_set('display_errors', 'On');

include_once ( '/data/project/fist/FileCandidates.php' ) ;

$config_file = '/data/project/fist/scripts/file_candidates/config.json' ;



function runSet ( $set , &$fc ) {
	$j = getSPARQL ( $set->sparql ) ;
	$last_query = '' ;

	foreach ( $j->results->bindings AS $b ) {
		$q = preg_replace ( '/^.+Q/' , 'Q' , $b->q->value ) ;

		// Construct query and skip if necessary
		$skip_item = false ;
		$query = [] ;
		foreach ( $set->parts AS $part ) {
			$var = $part->var ;
			$value = trim ( $b->$var->value ) ; # TODO ensure string
			if ( preg_match ( '/^Q\d+$/' , $value ) ) $value = '' ; # If value is a Wikidata item, pretend it's empty
			if ( $value == '' and $part->required ) $skip_item = true ;
			if ( $value == '' ) continue ;
			if ( $part->quote ) $value = "'" . $value . "'" ;
			$query[] = $value ;
		}
		if ( $skip_item ) continue ;
		$query = trim ( implode ( ' ' , $query ) ) ;
		if ( $query == '' or "$q$query" == $last_query ) continue ; // Paranoia
		$last_query = "$q$query" ;

		// Check if this item already has candidates; search only for ones that don't
		$existing_candidates = $fc->getFileCandidatesForItems ( [ $q ] ) ;
		if ( count($existing_candidates) > 0 ) continue ;

		if ( isset($set->commons) and $set->commons ) { // Fulltext search Commons
			$files = $fc->searchCommons ( $query ) ;
			foreach ( $files AS $file ) {
				if ( isset($set->exclude_files) ) {
					if ( preg_match ( $set->exclude_files , $file->title ) ) continue ;
				}
				$fc->addFile ( [
					'q' => $q ,
					'json' => $file ,
					'group' => $set->group ,
					'source' => 'COMMONS' ,
					'file_id' => $file->pageid
				] ) ;
			}
		}

		if ( isset($set->flickr) and $set->flickr ) { // Flickr search
			$files = $fc->searchFlickr ( $query ) ;
			foreach ( $files AS $file ) {
				if ( !isset($file['@attributes']) ) continue ; // Paranoia
				$fc->addFile ( [
					'q' => $q ,
					'json' => $file ,
					'group' => $set->group ,
					'source' => 'FLICKR' ,
					'file_id' => $file['@attributes']['id']
				] ) ;
			}
		}

	}
}


$sets = json_decode ( file_get_contents ( $config_file ) ) ;

$fc = new FileCandidates ;
foreach ( $sets AS $set ) {
	if ( isset($argv[1]) and strtoupper($argv[1]) != $set->group ) continue ; # Note that this allows for multiple configurations for the same group!
	runSet ( $set , $fc ) ;
}

?>