#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_ALL);
ini_set('display_errors', 'On');

include_once ( '/data/project/fist/FileCandidates.php' ) ;
$fc = new FileCandidates ;
$dbc = openDB ( 'commons' , 'wikimedia' ) ;

$limit = 1200000 ;
$offset = 0 ;

while ( 1 ) {
	$sparql = 'SELECT ?q ?name { ?q wdt:P31 wd:Q16521; wdt:P225 ?name ; wdt:P105 wd:Q7432 . OPTIONAL { ?q wdt:P18 ?img } FILTER(!bound(?img)) }' ;
	if ( isset($limit) ) $sparql .= " LIMIT $limit" ;
	if ( $offset > 0 ) $sparql .= " OFFSET $offset" ;
	$j = getSPARQL ( $sparql ) ;
	if ( $j === null ) die ( "SPARQL fail:\n$sparql\n" ) ;

	$cnt = 0 ;
	foreach ( $j->results->bindings AS $b ) {
		$cnt++ ;
		$q = preg_replace ( '/^.+Q/' , 'Q' , $b->q->value ) ;

		// Check if this item already has candidates; search only for ones that don't
		$existing_candidates = $fc->getFileCandidatesForItems ( [ $q ] ) ;
		if ( count($existing_candidates) > 0 ) continue ;

		$name = $b->name->value ;
		$query = '"' . $name . '"' ;

		// Flickr
		$files = $fc->searchFlickr ( $query ) ;
		foreach ( $files AS $file ) {
			print_r ( $file ) ;
			$fc->addFile ( [
				'q' => $q ,
				'json' => $file ,
				'group' => 'SPECIES' ,
				'source' => 'FLICKR' ,
				'file_id' => $file['@attributes']['id']
			] ) ;
		}

		// Categories on Commons
		$sql = "SELECT page_title FROM page,categorylinks WHERE cl_from=page_id AND page_namespace=6 AND cl_to='" . $fc->escape(str_replace(' ','_',$name)) . "' LIMIT 10" ;
		$result = getSQL ( $dbc , $sql ) ;
		while($o = $result->fetch_object()){
			$file = $fc->getCommonsImageInfo ( $o->page_title ) ;
			if ( !isset($file) ) continue ;
			$fc->addFile ( [
				'q' => $q ,
				'json' => $file ,
				'group' => 'SPECIES' ,
				'source' => 'COMMONS' ,
				'file_id' => $file->pageid
			] ) ;
		}

		// Fulltext search Commons
		$files = $fc->searchCommons ( $query ) ;
		foreach ( $files AS $file ) {
			$fc->addFile ( [
				'q' => $q ,
				'json' => $file ,
				'group' => 'SPECIES' ,
				'source' => 'COMMONS' ,
				'file_id' => $file->pageid
			] ) ;
		}

	}

	if ( !isset($limit) ) break ;
	if ( $cnt < $limit+$offset ) break ; // All done
	$offset += $limit ;
}

?>