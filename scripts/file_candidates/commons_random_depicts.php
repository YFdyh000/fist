#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); # |E_ALL
ini_set('display_errors', 'On');

include_once ( '/data/project/fist/FileCandidates.php' ) ;
$max_files_per_item = 4 ;
$iterations = 500 ; # * 50

$fc = new FileCandidates ;

$item_link_counterindications = [
	'Q4167836' , # category
	'Q11266439' , # template
	'Q202444' , # given name
	'Q12308941' , # male given name
	'Q11879590' , # female given name
	'Q202444' , # unisex name
	'Q4167410' , # disambiguation page
	'Q3624078' , # sovereign state
	'Q13406463' , # Wikimedia list article
	'Q14204246' , # Wikimedia project page
	'Q101352' , # family name
	'Q13442814' , # scholarly article
	'Q235729' # common year
] ;


function get_bad_qs() {
	global $fc , $bad_qs , $max_files_per_item ;
	$bad_qs = [] ;
	$sql = "SELECT q,count(*) AS cnt FROM file_candidates WHERE `group`='COMMONS_DEPICTS' GROUP BY q HAVING cnt>$max_files_per_item" ;
	$result = $fc->tfc->getSQL ( $fc->dbt , $sql ) ;
	while($o = $result->fetch_object()) $bad_qs["Q{$o->q}"] = "Q{$o->q}" ;
}

function reset_bad_qs() {
	global $fc , $bad_qs ;
	if ( count($bad_qs) == 0 ) return ;
	$qs = implode ( ',' , $bad_qs ) ;
	$qs = preg_replace ( '|Q|' , '' , $qs ) ;
	$sql = "UPDATE file_candidates SET `status`='SKIPPED' WHERE `status`='WAIT' AND q IN ({$qs}) AND `group`='COMMONS_DEPICTS'" ;
	$fc->tfc->getSQL ( $fc->dbt , $sql ) ;
}

function process_batch () {
	global $fc , $wikidata_api_url , $bad_qs , $item_link_counterindications ;
	$files = $fc->searchCommons ( 'haswbstatement:P180' , 6 , 50 , 'random') ;

	# Get image names to query in Wikidata, and Media IDs
	$image_names = [] ;
	$media_items = [] ;
	foreach ( $files as $page_id => $file ) {
		if ( !isset($file->imageinfo) ) continue ; # Paranoia to code around https://phabricator.wikimedia.org/T253373
		$filename = $fc->normalizeCommonsFilename ( $file->title ) ;
		$mid = "M{$page_id}" ;
		$image_names[] = $fc->escape ( $filename ) ;
		$media_items[] = $mid ;
		$mid2filename[$mid][$filename] = $filename ;
	}

	# Get MediaInfo for these files
	$wikidata_api_url_old = $wikidata_api_url ;
	$wikidata_api_url = "https://commons.wikimedia.org/w/api.php" ;
	$mid2wd = [] ;
	$wd2mid = [] ;
	$cowil = new WikidataItemList ;
	$cowil->loadItems ( $media_items ) ;
	foreach ( $media_items AS $mid ) {
		$i = $cowil->getItem ( $mid ) ;
		if ( !isset($i) ) {
			print "Could not get {$mid}\n" ;
			continue ;
		}
		$p180s = $i->getClaims ( 'P180' ) ;
		foreach ( $p180s AS $claims ) {
			foreach ( $claims AS $claim ) {
				if ( !isset($claim->datavalue) ) continue ;
				if ( !isset($claim->datavalue->value) ) continue ;
				if ( !isset($claim->datavalue->value->id) ) continue ;
				$q = $claim->datavalue->value->id ;
				$wd2mid[$q][$mid] = $mid ;
				$mid2wd[$mid][$q] = $q ;
			}
		}
	}
	$wikidata_api_url = $wikidata_api_url_old ;

	# Get the potential items without images
	$item_link_counterindications2 = implode ( "','" , $item_link_counterindications ) ;
	$items_without_p18 = [] ;
	$sql = implode("','", array_keys($wd2mid) ) ;
	$sql = "SELECT DISTINCT page_title FROM page WHERE page_namespace=0 AND page_title IN ('{$sql}')"  ;
	$sql .= " AND NOT EXISTS (SELECT * FROM pagelinks WHERE pl_namespace=120 AND pl_title='P18' AND pl_from=page_id)" ; # No image
	$sql .= " AND NOT EXISTS (SELECT * FROM pagelinks WHERE pl_namespace=0 AND pl_title IN ('{$item_link_counterindications2}') AND pl_from=page_id)" ; # No category etc
	$sql .= " AND NOT EXISTS (SELECT * FROM pagelinks WHERE pl_namespace=120 AND pl_title='P279' AND pl_from=page_id)" ; # No subclass
	$sql .= " AND EXISTS (SELECT * FROM pagelinks WHERE pl_namespace=120 AND pl_title='P31' AND pl_from=page_id)" ; # But P31
	$result = $fc->tfc->getSQL ( $fc->dbw , $sql ) ;
	while($o = $result->fetch_object()) {
		$q = $o->page_title ;
		if ( !isset($wd2mid[$q]) ) continue ;
		$items_without_p18[] = $q ;
	}

	# Get existing image links for the ones without image, to exclude logos etc
	$item_using_file = [] ;
	$sql = implode ( "','" , $items_without_p18 ) ;
	$sql = "SELECT page_title,il_to FROM page,imagelinks WHERE il_from=page_id AND page_namespace=0 AND page_title IN ('{$sql}')" ;
	$result = $fc->tfc->getSQL ( $fc->dbw , $sql ) ;
	while($o = $result->fetch_object()) {
		$filename = $fc->normalizeCommonsFilename ( $o->il_to ) ;
		$q = $o->page_title ;
		$item_using_file["{$q}:{$filename}"] = 1 ;
	}

	$item_using_file = [] ;
	foreach ( $items_without_p18 AS $q ) {
		if ( in_array ( $q , $bad_qs ) ) continue ;
		foreach ( $wd2mid[$q] as $mid ) {
			foreach ( $mid2filename[$mid] as $filename ) {
				$k = "{$q}:{$filename}" ;
				if ( isset($item_using_file[$k]) ) continue ;
				$item_using_file[$k] = 1 ;
				$page_id = substr($mid,1) * 1 ;
				#print "Potential image for page {$page_id} ({$q}): $filename\n" ;
				$file = $files->$page_id ;
				if ( !isset($file) ) {
					print "No file for page {$page_id}\n" ;
					continue ;
				}
					$fc->addFile ( [
						'q' => $q ,
						'json' => $file ,
						'group' => 'COMMONS_DEPICTS' ,
						'source' => 'COMMONS' ,
						'file_id' => $file->pageid
					] ) ;

			}
		}
	}
}

get_bad_qs() ;
reset_bad_qs() ;

for ( $cnt = 0 ; $cnt < $iterations ; $cnt++ ) process_batch() ;

get_bad_qs() ;
reset_bad_qs() ;

?>