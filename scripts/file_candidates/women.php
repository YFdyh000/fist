#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_ALL);
ini_set('display_errors', 'On');

$use_flickr = false ;

include_once ( '/data/project/fist/FileCandidates.php' ) ;
$fc = new FileCandidates ;

function scanSPARQLresults ( $set ) {
	global $fc , $use_flickr ;
	$sparql = $set['sparql'] ;
	if ( isset($set['limit']) ) $sparql .= " LIMIT {$set['limit']}" ;

	$j = getSPARQL ( $sparql ) ;
	if ( $j === null ) {
		print "SPARQL fail for {$set['name']}\n" ;
		return ;
	}

	$cnt = 0 ;
	foreach ( $j->results->bindings AS $b ) {
		$cnt++ ;
		$q = preg_replace ( '/^.+Q/' , 'Q' , $b->q->value ) ;

		// Check if this item already has candidates; search only for ones that don't
		$existing_candidates = $fc->getFileCandidatesForItems ( [ $q ] ) ;
		if ( count($existing_candidates) > 0 ) continue ;

		$name = $b->name->value ;
		$query = '"' . $name . '"' ;

		// Flickr
		if ( $use_flickr ) {
			$files = $fc->searchFlickr ( $query ) ;
			foreach ( $files AS $file ) {

				$fc->addFile ( [
					'q' => $q ,
					'json' => $file ,
					'group' => $set['group'] ,
					'source' => 'FLICKR' ,
					'file_id' => $file['@attributes']['id']
				] ) ;
			}
		}

		// Fulltext search Commons
		$files = $fc->searchCommons ( $query ) ;
		foreach ( $files AS $file ) {
			$fc->addFile ( [
				'q' => $q ,
				'json' => $file ,
				'group' => $set['group'] ,
				'source' => 'COMMONS' ,
				'file_id' => $file->pageid
			] ) ;
		}
	}
}


$sets = [
	[
		'name' => 'Three-letter women' ,
		'group' => 'WOMEN' ,
		'limit' => 30000 ,
		'sparql' => 'SELECT ?q ?name { ?q wdt:P31 wd:Q5 ; wdt:P21 wd:Q6581072 ; rdfs:label ?name. FILTER(LANG(?name) = "en") . FILTER REGEX(STR(?name), "^\\\\S+ \\\\S{4,} \\\\S+$") . OPTIONAL { ?q wdt:P18 ?img } FILTER(!bound(?img)) }'
	]
] ;

foreach ( $sets AS $set ) {
	scanSPARQLresults ( $set ) ;
}

?>