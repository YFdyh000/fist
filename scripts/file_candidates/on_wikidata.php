#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_ALL);
ini_set('display_errors', 'On');

include_once ( '/data/project/fist/FileCandidates.php' ) ;

$template2sparql = [ # Template names need to be DB safe!
	'Listed_building_England' => 'select ?q { ?q wdt:P1216 "$ID" }' ,
	'Listed_building_Wales' => 'select ?q { ?q wdt:P1459 "$ID" }' ,
	'Listed_building_Northern_Ireland' => 'select ?q { ?q wdt:P1460 "$ID" }' ,
	'Listed_building_Scotland' => 'select ?q { ?q wdt:P709 "$ID" }'
] ;

$config_file = '/data/project/fist/scripts/file_candidates/on_wikidata.json' ;
$config_json = json_decode ( file_get_contents ( $config_file ) ) ;


function guessItemForFile ( $file ) {
	global $fc , $template2sparql ;
	$q = '' ;
	$wikitext = $fc->tfc->getWikiPageText ( 'commonswiki' , 'File:'.$file ) ;
	$wikitext = preg_replace ( '/\s+/' , ' ' , $wikitext ) ;
	$r = 0 ;
	if ( $r == 0 ) $r = preg_match ( '/\{\{ *[oO]n[ _]Wikidata *\| *([Qq]\d+)/' , $wikitext , $m ) ;
	if ( $r == 0 ) $r = preg_match ( '/\{\{ *Wikidata *\| *([Qq]\d+)/' , $wikitext , $m ) ;
	if ( $r == 0 ) {
		unset ( $m ) ;
		foreach ( $template2sparql AS $template => $sparql ) {
			$template = str_replace ( ' ' , '_' , $template ) ;
			$pattern = str_replace ( '_' , '[ _]' , $template ) ;
			$pattern = '/\{\{ *' . $pattern . ' *\| *([^ \|\}]+)/i' ;
			$r = preg_match ( $pattern , $wikitext , $m2 ) ;
			if ( $r == 0 ) continue ;
			$id = $m2[1] ;
			$id = preg_replace ( '/^.+=/' , '' , $id ) ; # Remove template parameter, if any
			$sparql = str_replace ( '$ID' , $id , $sparql ) ;
			$items = $fc->tfc->getSPARQLitems ( $sparql ) ;
			if ( count($items) != 1 ) continue ;
			$q = $items[0] ;
			return $q ;
		}
	}
	if ( $q == '' and $r == 0 ) return '' ;
	if ( $q == '' and isset($m) ) $q = $m[1] ;
	$q = preg_replace ( '/^.+=/' , '' , strtoupper ( $q ) ) ; # Remove template parameter, if any
	$q = trim ( $q ) ;
	if ( !preg_match ( '/^Q\d+$/' , $q ) ) return '' ; # Paranoia
	return $q ;
}

$fc = new FileCandidates ;


if ( isset($argv[1]) and $argv[1] == 'oneoff' ) { # This was used once to fix erroneous data from an earlier bug. Keeping it here for sentimental reasons.
	$sql = "select * from file_candidates where `group`='ON WIKIDATA' and `status`='WAIT'" ;
	$result = $fc->tfc->getSQL ( $fc->dbt , $sql ) ;
	while ($o = $result->fetch_object()) {
		$j = json_decode ( $o->json ) ;
		$file = preg_replace ( '/^File:/' , '' , $j->title ) ;
		$q = guessItemForFile ( $file ) ;
		if ( $q == '' ) {
			$sql = "DELETE FROM file_candidates WHERE id={$o->id}" ;
			$fc->tfc->getSQL ( $fc->dbt , $sql ) ;
		} else {
			$q = preg_replace ( '/\D/' , '' , $q ) * 1 ;
			if ( $q == $o->q ) continue ;
			$sql = "UPDATE file_candidates SET q={$q} WHERE id={$o->id}" ;
			$fc->tfc->getSQL ( $fc->dbt , $sql ) ;
		}
	}
	exit(0);
}

$sql = "SELECT DISTINCT page_title FROM commonswiki_p.templatelinks,commonswiki_p.page
WHERE page_id=tl_from
AND tl_namespace=10
AND tl_from_namespace=6
AND `page_touched`>='{$config_json->last_page_touched}'
AND tl_title IN ('On_Wikidata','" . str_replace(' ','_',implode("','",array_keys($template2sparql))) . "')
AND NOT EXISTS (SELECT * FROM imagelinks WHERE il_to=page_title AND il_from_namespace=0 LIMIT 1)
" ; # ORDER BY rand()

$config_json->last_page_touched = date ( 'YmdHis' , time() - 60*10 ) ; # 10 min ago

$files_processed = 0 ;
$result = $fc->tfc->getSQL ( $fc->dbw , $sql ) ;
while ($o = $result->fetch_object()) {
	$files_processed++ ;
	$q = '' ;
	$file = $o->page_title ;
#print "Checking {$file}...\n" ;

	$q = guessItemForFile ( $file ) ;
	if ( $q == '' ) continue ;


	# Follow redirect, if necessary
	$sql = "SELECT * FROM page,pagelinks WHERE page_namespace=0 AND page_title='{$q}' AND page_is_redirect=1 AND pl_from=page_id AND pl_namespace=0" ;
	$result2 = $fc->tfc->getSQL ( $fc->dbw , $sql ) ;
	if ($o2 = $result2->fetch_object()) $q = $o2->pl_title ;

	# Check for image
	if ( $fc->doesItemHaveImage ( $q ) ) continue ;

	# Add image candidate
	$j = $fc->getCommonsImageInfo ( $file ) ;
	if ( $j === null ) continue ; // Paranoia
#	if ( $fc->doesFileCandidateExists ( 'COMMONS' , $j->pageid ) ) continue ;
	$fc->addFile ( [
		'q' => $q ,
		'json' => $j ,
		'group' => 'ON WIKIDATA' ,
		'source' => 'COMMONS' ,
		'comment' => 'Via {{On Wikidata}}' ,
		'file_id' => $j->pageid ,
		'file_type' => 'IMAGE'
	] ) ;
}

print "Files processed: {$files_processed}\n" ;
file_put_contents ( $config_file , json_encode ( $config_json ) ) ;

?>