#!/usr/bin/php
<?php

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( '/data/project/fist/FileCandidates.php') ;

$max_files_per_search_result = 20 ;

$fc = new FileCandidates ;

$sparql = '
SELECT ?painting ?paintingLabel ?creator ?creatorLabel {
  ?painting wdt:P31 wd:Q3305213 .
  ?painting wdt:P170 ?creator .
  ?painting wdt:P571 ?date FILTER (?date < "1830-01-01T00:00:00Z"^^xsd:dateTime)
  MINUS { ?painting wdt:P18 [] }
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
  }
' ;

$j = getSPARQL ( $sparql ) ;

foreach ( $j->results->bindings AS $b ) {
	$q_painting = preg_replace ( '/^.+\/Q/' , 'Q' , $b->painting->value ) ;
	$q_creator = preg_replace ( '/^.+\/Q/' , 'Q' , $b->creator->value ) ;
	if ( $q_creator == 'Q4233718' ) continue ; // "anonymous"
	$label_painting = $b->paintingLabel->value ;
	$label_creator = $b->creatorLabel->value ;
	$query = '"' . $label_painting . '" ' . $label_creator ;
	$files = $fc->searchCommons ( $query , 6 ) ;
	if ( count($files) > $max_files_per_search_result ) continue ;
	foreach ( $files AS $file_id => $file ) {
		if ( preg_match ( '/\.(pdf|svg|og.|mp.)$/i' , $file->title ) ) continue ;
		$fc->addFile ( [
			'q' => $q_painting ,
			'group' => 'PAINTING' ,
			'source' => 'COMMONS' ,
			'file_id' => $file_id ,
			'json' => $file
		] ) ;
	}
}

?>