#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_ALL);
ini_set('display_errors', 'On');

include_once ( '/data/project/fist/FileCandidates.php' ) ;
$fc = new FileCandidates ;
$fc->dbc = openDB ( 'commons' , 'wikimedia' ) ;

// Try to find Flickr files on Commons, and replace them in file_candidates


$attr = '@attributes' ;
$sql = 'SELECT id,json,file_id FROM file_candidates WHERE source="FLICKR" AND status="WAIT"' ;
$result = getSQL ( $fc->dbt , $sql ) ;
while($o = $result->fetch_object()) {
	$j = json_decode ( $o->json ) ;

	if ( !isset($j->$attr) ) continue ; # TODO error

	$urls = [] ;
	$urls[] = "http://flickr.com/photos/" . $j->$attr->owner . '/' . $o->file_id ;
	$urls[] = "https://flickr.com/photos/" . $j->$attr->owner . '/' . $o->file_id ;
	$urls[] = "http://www.flickr.com/photos/" . $j->$attr->owner . '/' . $o->file_id ;
	$urls[] = "https://www.flickr.com/photos/" . $j->$attr->owner . '/' . $o->file_id ;
	foreach ( $urls AS $k => $v ) {
		if ( preg_match ( '/\/$/',$v) ) break ;
		$urls[] = "$v/" ;
	}

	$sql = "SELECT * FROM page,externallinks WHERE page_id=el_from AND page_namespace=6 AND el_to IN ('" . implode ( "','",$urls) . "')" ;
	$commons_files = [] ;
	$result2 = getSQL ( $fc->dbc , $sql ) ;
	while($o2 = $result2->fetch_object()) $commons_files[] = $o2 ;
	if ( count($commons_files) != 1 ) continue ;

	$o2 = $commons_files[0] ;
	$file = $fc->getCommonsImageInfo ( $o2->page_title ) ;
	if ( !isset($file) ) continue ; # Paranoia
	$j = $fc->escape ( json_encode ( $file ) ) ;
	if ( $j == '' ) continue ;
	$sql = "UPDATE IGNORE file_candidates SET `file_id`='{$file->pageid}',`source`='COMMONS',`json`='$j' WHERE id={$o->id} AND `source`='FLICKR' AND `file_id`='{$o->file_id}'" ;
#	print "$sql\n" ;
	getSQL ( $fc->dbt , $sql ) ;
}

?>