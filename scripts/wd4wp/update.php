#!/usr/bin/php
<?PHP

require_once ( "/data/project/fist/public_html/php/ToolforgeCommon.php" ) ;

class WD4WP {
	public $langs = [ 'de' , 'es' , 'it' , 'ru' , 'nl' , 'ca' , 'en' , 'cy' , 'pt' , 'fr' ] ;
	public $tfc ;
	protected $dbt ;

	function __construct() {
		$this->tfc = new ToolforgeCommon ( 'wd4wp' ) ;
		$this->dbt = $this->tfc->openDBtool ( 'wd4wp_p' ) ;
	}

	protected function normalize_title ( $title ) {
		return str_replace ( ' ' , '_' , ucfirst ( trim ( str_replace ( '_' , ' ' , $title ) ) ) ) ;
	}

	protected function escape ( $s ) {
		return $this->dbt->real_escape_string ( $s ) ;
	}

	protected function get_or_create_wiki_id ( $wiki ) {
		$wiki = $this->escape ( $wiki ) ;
		$sql = "SELECT `id` FROM `wiki` WHERE `name`='{$wiki}'" ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql ) ;
		while($o = $result->fetch_object()) return $o->id ;
		$sql = "INSERT IGNORE INTO `wiki` (`name`) VALUES ('{$wiki}')" ;
		$this->tfc->getSQL ( $this->dbt , $sql ) ;
		return $this->dbt->insert_id ;
	}

	protected function is_valid_image ( $image ) {
		if ( preg_match ( '|\.svg$|i' , $image ) ) return false ; # Ignore SVG
		# TODO more?
		return true ;
	}

	protected function add_articles ( $wiki_id , $q2article ) {
		$items = array_keys ( $q2article ) ;
		$sparql = "SELECT ?q ?image ?instance_of { VALUES ?q { wd:" . implode ( ' wd:' , $items ) . " } ?q wdt:P18 ?image OPTIONAL { ?q wdt:P31 ?instance_of } }" ;
		$j = $this->tfc->getSPARQL ( $sparql ) ;
		if ( !isset($j) or !isset($j->results) or !isset($j->results->bindings) ) return ;
		$data = [] ;
		foreach ( $j->results->bindings AS $v ) {
			$q = preg_replace ( '/^.+\//' , '' , $v->q->value ) ;
			$img = $this->normalize_title ( urldecode ( preg_replace ( '/^.+\//' , '' , $v->image->value ) ) ) ;
			if ( !$this->is_valid_image($img) ) continue ; # Paranoia
			if ( !isset($data[$q]) ) $data[$q] = [ 'q' => $q , 'image' => $img , 'title' => $q2article[$q] , 'io' => [] ] ;
			if ( isset($v->instance_of) ) $data[$q]['io'][] = preg_replace ( '/^.+\//' , '' , $v->instance_of->value ) ;
		}

		foreach ( $data AS $q => $page ) {
			$title = $this->escape ( $page['title'] ) ;
			$image = $this->escape ( $page['image'] ) ;
			$q = preg_replace ( '|\D|' , '' , $page['q'] ) * 1 ;
			$sql = "INSERT IGNORE INTO `page` (`wiki`,`title`,`q`,`image`) VALUES ({$wiki_id},'{$title}',{$q},'{$image}')" ;
			$this->tfc->getSQL ( $this->dbt , $sql ) ;
			$page_id = $this->dbt->insert_id ;
			if ( count($page['io']) == 0 ) continue ;
			$page_instance = [] ;
			foreach ( $page['io'] AS $p31 ) {
				$p31 = preg_replace ( '|\D|' , '' , $p31 ) * 1 ;
				$page_instance[] = "({$page_id},{$p31})" ;
			}
			$sql = "INSERT IGNORE INTO `page_instance` (`page_id`,`p31`) VALUES " . implode ( ',' , $page_instance ) ;
			$this->tfc->getSQL ( $this->dbt , $sql ) ;
		}
	}

	public function update_all () {
		foreach ( $this->langs AS $lang ) {
			$this->update_wiki ( "{$lang}wiki" ) ;
		}
	}

	protected function get_candidate_articles ( $wiki , &$existing_articles ) {
		$q2article = [] ;
		$sql = 'SELECT local_page.page_title AS lp_title,pp1.pp_value AS q FROM page local_page,page_props pp1
		WHERE local_page.page_is_redirect=0 AND local_page.page_namespace=0 AND NOT EXISTS (SELECT * FROM page_props pp2 WHERE local_page.page_id=pp2.pp_page AND pp2.pp_propname IN ("page_image","page_image_free") ) 
		AND local_page.page_id=pp1.pp_page AND pp1.pp_propname="wikibase_item" 
		AND EXISTS (SELECT * FROM wikidatawiki_p.page wd_page,wikidatawiki_p.pagelinks WHERE wd_page.page_namespace=0 AND wd_page.page_title=pp1.pp_value AND pl_from=wd_page.page_id AND pl_namespace=120 AND pl_title="P18")' ;
		$dbw = $this->tfc->openDBwiki ( $wiki ) ;
		$result = $this->tfc->getSQL ( $dbw , $sql ) ;
		while($o = $result->fetch_object()) {
			$title = $this->normalize_title ( $o->lp_title ) ;
			if ( isset($existing_articles[$title]) ) {
				$existing_articles[$title] = 'FOUND' ;
				continue ;
			}
			$q = $o->q ;
			if ( isset ( $q2article[$q] ) ) continue ;
			$q2article[$q] = $this->normalize_title ( $title ) ;
		}
		$dbw->close() ;
		return $q2article ;
	}

	protected function remove_non_candidates ( $wiki_id , $existing_articles ) {
		$titles_to_remove = [] ;
		foreach ( $existing_articles AS $title => $status ) {
			if ( $status == 'FOUND' ) continue ;
			$titles_to_remove[] = $this->escape($title) ;
		}
		if ( count($titles_to_remove) == 0 ) return ;

		$sql = "UPDATE `page` SET `status`='NO_LONGER_VALID' WHERE `wiki`={$wiki_id} AND `title` IN ('".implode("','",$titles_to_remove)."') AND `status`='TODO'" ;
		$this->tfc->getSQL ( $this->dbt , $sql ) ; # TODO
	}

	public function update_wiki ( $wiki ) {
		$wiki_id = $this->get_or_create_wiki_id ( $wiki ) ;
		$this->set_last_update_now ( $wiki_id ) ;

		$existing_articles = [] ;
		$sql = "SELECT `title`,`status` FROM `page` WHERE `wiki`={$wiki_id}" ; #  AND `status`='IGNORE'
		$result = $this->tfc->getSQL ( $this->dbt , $sql ) ;
		while($o = $result->fetch_object()) $existing_articles[$o->title] = $o->status ;

		$q2article = $this->get_candidate_articles ( $wiki , $existing_articles ) ;
		$this->remove_non_candidates ( $wiki_id , $existing_articles ) ;
		unset ( $existing_articles ) ;

		$chunks = array_chunk ( $q2article , 100 , true ) ;
		unset ( $q2article ) ;
		foreach ( $chunks AS $q2article ) $this->add_articles ( $wiki_id , $q2article ) ;
	}

	protected function set_last_update_now ( $wiki_id ) {
		$sql = "UPDATE `wiki` SET `last_update`=now() WHERE id={$wiki_id}" ;
		$this->tfc->getSQL ( $this->dbt , $sql ) ;
	}

	public function import_ignore ( $wiki ) {
		$wiki_id = $this->get_or_create_wiki_id ( $wiki ) ;
		$ignore = explode ( "\n" , trim ( file_get_contents ( "{$wiki}.has_image" ) ) ) ;
		$to_add = [] ;
		foreach ( $ignore AS $title ) {
			$title = $this->escape ( $this->normalize_title ( $title ) ) ;
			if ( $title == '' ) continue ; # Paranoia
			$to_add[] = "({$wiki_id},'{$title}','IGNORE')" ;
		}
		if ( count ( $to_add ) == 0 ) return ;
		$sql = "INSERT IGNORE INTO `page` (`wiki`,`title`,`status`) VALUES " . implode ( ',' , $to_add );
		$this->tfc->getSQL ( $this->dbt , $sql ) ;
	}

	public function update_next_wiki() {
		$sql = "SELECT `name` FROM `wiki` ORDER BY `last_update` LIMIT 1" ;
		$result = $this->tfc->getSQL ( $this->dbt , $sql ) ;
		$o = $result->fetch_object() ;
		$this->update_wiki ( $o->name ) ;
	}

}


$wd4wp = new WD4WP ;
#$wd4wp->import_ignore ( $argv[1] ) ;
if ( isset($argv[1]) ) $wd4wp->update_wiki ( $argv[1] ) ;
else $wd4wp->update_next_wiki() ;

?>