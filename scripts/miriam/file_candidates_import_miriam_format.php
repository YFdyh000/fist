#!/usr/bin/php
<?php

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( '/data/project/fist/FileCandidates.php') ;

#$filename = 'people_image_candidates.csv' ; $group = 'HUMAN' ;
$filename = 'species_image_candidates.csv' ; $group = 'SPECIES' ;
$source = 'COMMONS' ;
$comment = 'Miriam' ;

$fc = new FileCandidates ;
$timestamp = $fc->getTimestamp() ;

/*
FORMAT :
[Wikidata Qid, without Q] - [number of files] - [filename without File:|rating as float]
*/
$rows = explode ( "\n" , file_get_contents($filename) ) ;

foreach ( $rows AS $row ) {
	if ( $row == '' ) continue ;
	$parts = explode ( "\t" , $row ) ;
	$q = array_shift ( $parts ) * 1 ;

	$existing = [] ;
	$sql = "SELECT * FROM file_candidates WHERE `group`='$group' AND `source`='$source' AND q=$q" ;
	$result = getSQL ( $fc->dbt , $sql ) ;
	while($o = $result->fetch_object()) {
		$j = json_decode ( $o->json ) ;
		if ( $j == null ) continue ; // Broken JSON
		$filename = $fc->normalizeCommonsFilename ( $j->title ) ;
		$existing[$filename] = $o ;
	}


	$num_files = array_shift ( $parts ) ; // Ignore
	while ( count($parts) > 0 ) {
		list ( $filename , $rating ) = explode ( '|' , array_shift ( $parts ) ) ;
		$filename = $fc->normalizeCommonsFilename ( $filename ) ;
		$rating *= 1 ;
		if (isset($existing[$filename])) {
			$sql = "UPDATE file_candidates SET comment='$comment [$timestamp]',rating=$rating WHERE id={$existing[$filename]->id}" ;
			$sql .= " AND rating IS NULL" ; // Do not overwrite
			getSQL ( $fc->dbt , $sql ) ;
		} else {
			$ii = $fc->getCommonsImageInfo ( $filename ) ;
			if ( !isset($ii) ) continue ; 
			$fc->addFile ( [
				'q' => $q ,
				'group' => 'HUMAN' ,
				'source' => 'COMMONS' ,
				'file_id' => $ii->pageid ,
				'rating' => $rating ,
				'comment' => "$comment [$timestamp]" ,
				'file_type' => $fc->getFileTypeByExtension ( $filename ) ,
				'json' => $ii
			] ) ;
		}
	}
}

?>