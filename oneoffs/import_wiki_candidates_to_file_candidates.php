#!/usr/bin/php
<?PHP
require_once ( '/data/project/fist/FileCandidates.php') ;

$fc = new FileCandidates ;
$sql = "SELECT * FROM wiki_candidates WHERE `group`='Q5' AND wiki_count>1 AND `status`='OPEN'" ;
$result = getSQL ( $fc->dbt , $sql , 2 ) ;
while($o = $result->fetch_object()){

/*
	// Check if we already cover that item; maybe too stringent!
	$sql = "SELECT * FROM file_candidates WHERE q={$o->q}" ;
	$result2 = getSQL ( $fc->dbt , $sql , 2 ) ;
	if($o2 = $result2->fetch_object()) continue ;
*/
	
	$fc->wil->loadItem ( $o->q ) ;
	$i = $fc->wil->getItem ( $o->q ) ;
	if ( !isset($i) ) continue ; // No such item
	if ( $i->hasClaims('P18') ) continue ; // Has image
	if ( !$i->hasTarget('P31','Q5') ) continue ; // Not a human

	$url = "https://commons.wikimedia.org/w/api.php?action=query&format=json&prop=imageinfo&iiprop=url&iiurlwidth=120&iiurlheight=120&titles=File:".myurlencode($o->file) ;

	$fail = false ;
	$data = file_get_contents ( $url ) ;
	if ( $data == null || $data == '' ) $fail = true ;
	else {
		$data = json_decode ( $data ) ;
		if ( !isset($data->query) ) $fail = true ;
		else if ( !isset($data->query->pages) ) $fail = true ;
		else if ( count($data->query->pages) == 0 ) $fail = true ;
	}
	
	if ( $fail ) {
		//print "FAIL {$o->file}\n" ;
	} else {
		foreach ( $data->query->pages AS $page_id => $page ) {
			if ( isset($page->missing) ) continue ;
			$fc->addFile ( [
				"q" => $o->q ,
				"json" =>  $page ,
				"group" => 'HUMAN' ,
				"source" => 'COMMONS' ,
				"comment" => 'import from wiki_candidates' ,
				"file_id" => $page_id
			 ] ) ;
		}
	}

}

?>